import {AbstractFragment} from "cordova-sites/dist/client/js/Context/AbstractFragment";
import view from "../../../html/Fragments/statistic/exerciseStatisticsOverviewFragment.html";
import {DateHelper} from "js-helper/dist/shared/DateHelper";

export class ExerciseStatisticsOverviewFragment extends AbstractFragment{
    constructor(site) {
        super(site, view);
    }

    async onViewLoaded() {
        let res = super.onViewLoaded();

        this._numberExercisesElement = this.findBy("#number-exercises");
        this._numberWrongAnswersElement = this.findBy("#number-wrong-answers");
        this._numberExerciseInProgressElement = this.findBy("#number-exercises-in-progress");
        this._numberExerciseDone = this.findBy("#number-exercises-done");
        this._numberExerciseAborted = this.findBy("#number-exercises-aborted");
        this._averageTimeExerciseDone = this.findBy("#average-time-exercises-done");
        this._averageTimeExerciseAborted = this.findBy("#average-time-exercises-aborted");
        this._averageTimeExercise = this.findBy("#average-time-exercises");

        return res;
    }

    async setStatistic(statistic) {
        await this._viewLoadedPromise;

        this._numberExercisesElement.innerText = statistic["number-exercises"];
        this._numberWrongAnswersElement.innerText = statistic["number-wrong-answers"];
        this._numberExerciseInProgressElement.innerText = statistic["number-exercises-in-progress"];
        this._numberExerciseDone.innerText = statistic["number-exercises-done"];
        this._numberExerciseAborted.innerText = statistic["number-exercises-aborted"];

        let avgTimeDone = new Date();
        avgTimeDone.setTime(statistic["average-time-exercises-done"]);
        let avgTimeAborted = new Date();
        avgTimeAborted.setTime(statistic["average-time-exercises-aborted"]);
        let avgTime = new Date();
        avgTime.setTime(statistic["average-time-exercises"]);

        this._averageTimeExerciseDone.innerText = DateHelper.strftime("%H:%M:%S", avgTimeDone, true);
        this._averageTimeExerciseAborted.innerText = DateHelper.strftime("%H:%M:%S", avgTimeAborted, true);
        this._averageTimeExercise.innerText = DateHelper.strftime("%H:%M:%S", avgTime, true);
    }
}