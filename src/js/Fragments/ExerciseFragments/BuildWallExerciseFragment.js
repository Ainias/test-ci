import {ExerciseDescriptionFragment} from "./SubFragments/ExerciseDescriptionFragment";
import {SwipeFragment} from "cordova-sites/dist/client";
import {BuildWallFragment} from "./SubFragments/BuildWallExercise/BuildWallFragment";

export class BuildWallExerciseFragment extends SwipeFragment {
    constructor(site) {
        super(site);
        console.log("BuildWall Exercise Fragment");
        this._exerciseDescriptionFragment = new ExerciseDescriptionFragment(this.getSite());
        this._buildwallfragment = new BuildWallFragment(this.getSite());

        this.addFragment(this._exerciseDescriptionFragment);
        this.addFragment(this._buildwallfragment);
    }

    async setData(data) {
        await this._exerciseDescriptionFragment.setData(data);
        await this._buildwallfragment.setData(data);
    }

    async setElement(e) {
        console.log(e);
        await this._exerciseDescriptionFragment.setElement(e);
    }

    previousFragment() {
        if (this._activeIndex > 0) {
            super.previousFragment();
        } else {
            console.log(this.getSite());
            this.getSite().goBack();
        }
    }
}
