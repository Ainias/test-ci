import {ExerciseDescriptionFragment} from "./SubFragments/ExerciseDescriptionFragment";
import {SwipeFragment} from "cordova-sites/dist/client";
import {ExerciseHelper} from "../../Helpers/ExerciseHelper";
import {BuildWallFragment} from "./SubFragments/BuildWallExercise/BuildWallFragment";

export class BuildWallDimension extends SwipeFragment {
    constructor(site) {
        super(site);

        this._ExerciseDescriptionFragment = new ExerciseDescriptionFragment(this.getSite());
        this._buildwallfragment = new BuildWallFragment(this.getSite());
        this._element = null;
        this.addFragment(this._ExerciseDescriptionFragment);
        this.addFragment(this._buildwallfragment);
    }

    async setElement(element) {
        //console.log("Build Wall for the Night King")
        this._element = element;

        let progress = await ExerciseHelper.getProgressFor(element);

        let data = progress.getData();
            // + parseInt(data["exercise-breadth-wall"]);
        data["length"]=parseFloat(data["exercise-length-wall"]);
        data ["breadth"]=parseFloat(data["exercise-breadth-wall"]);

        console.log("Have to build wall of length "+data["length"]+" and breadth of "+data["breadth"]);

        data["exercise-description"] = "This exercise requires you to virtually build a wall of length "+data["length"]+" and breadth "+data["breadth"];
        data["exercise-understood-exercise-question"] = "What is the purpose of the following exercise?";
        data["exercise-understood-exercise-answer-1"] = "Virtually build a wall of the above stated dimensions";
        data["exercise-understood-exercise-answer-2"] = "State the length and breadth of the wall";
        data["exercise-understood-exercise-answer-3"] = "Calculate the length and breadth of the wall";
        data["exercise-understood-exercise-answer-4"] = "Calculate the surface area and volume of the wall with given dimensions";
        data["exercise-understood-exercise-correct"] = "1";
        progress.setData(data);

        await this._ExerciseDescriptionFragment.setElement(element);
        await this._buildwallfragment.setElement(element);
    }

    goBack() {
        return this.previousFragment();
    }

    previousFragment() {
        if (this._activeIndex > 0) {
            super.previousFragment();
        } else {
            this.getSite().goBack();
        }
    }
}

