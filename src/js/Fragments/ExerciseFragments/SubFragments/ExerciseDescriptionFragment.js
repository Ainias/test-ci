import view from "../../../../html/Fragments/ExerciseFragments/Exercises/SubFragments/exerciseDescriptionFragment.html";
import {Helper, SwipeChildFragment, Toast} from "cordova-sites/dist/client";
import {ExerciseHelper} from "../../../Helpers/ExerciseHelper";

export class ExerciseDescriptionFragment extends SwipeChildFragment {
    constructor(site) {
        super(site, view);
        this._answerIndexes = [];
        this._rightAnswer = null;
    }

    async setElement(element) {
        this._exercise = element;
        let progress = await ExerciseHelper.getProgressFor(this._exercise);

        await this.setData(progress.getData());
        await this.setState(progress.state);
    }

    async onViewLoaded() {
        let res = super.onViewLoaded();
        this.findBy(".start-exercise-button").addEventListener("click", () => {
            this._checkAnswer();
        });

        return res;
    }

    async onSwipeRight() {
    }

    async onSwipeLeft() {
        await this._checkAnswer();
    }

    async _checkAnswer() {
        if (Helper.isNotNull(this._rightAnswer) && this._answerIndexes.length > 0) {
            let selectedAnswer = this.findBy("[name='exercise-understood-exercise-correct']:checked");
            let progress = await ExerciseHelper.getProgressFor(this._exercise);
            if (Helper.isNull(selectedAnswer)) {
                let data = progress.getData();
                ExerciseHelper.addWrongAnswer(progress, "exercise-understood-exercise-correct", null, data["exercise-understood-exercise-answer-"+this._rightAnswer]);

                new Toast("please select an answer").show();
                return;
            }
            if (selectedAnswer.value >= this._answerIndexes.length
                || this._answerIndexes[selectedAnswer.value] !== this._rightAnswer) {

                let data = progress.getData();
                ExerciseHelper.addWrongAnswer(progress, "exercise-understood-exercise-correct", data["exercise-understood-exercise-answer-"+(this._answerIndexes[selectedAnswer.value])], data["exercise-understood-exercise-answer-"+this._rightAnswer]);

                new Toast("wrong answer").show();
                return;
            }

            let state = progress.state;
            state["exercise-understood-exercise-selected"] = this._rightAnswer;
            progress.state = state;
            await this.getSite().showLoadingSymbol();
            await progress.save();
            await this.getSite().removeLoadingSymbol();

            this.nextFragment();
        }
    }

    async setData(data) {
        this._descriptionElement = document.createTextNode(data["exercise-description"]);
        (await this.findBy(".exercise-description", null, true)).appendChild(this._descriptionElement);

        this._image = data["exercise-image"];
        this._imageDescription = data["exercise-image-description"];

        if (!Helper.imageUrlIsEmpty(this._image)) {
            let img = await this.findBy(".exercise-image", null, true);
            img.src = this._image;
            img.title = this._imageDescription;
            img.alt = this._imageDescription;

            (await this.findBy(".exercise-image-description", null, true)).appendChild(document.createTextNode(this._imageDescription));
        }

        (await this.findBy(".exercise-understood-exercise-question", null, true))
            .appendChild(document.createTextNode(data["exercise-understood-exercise-question"]));

        this._answerIndexes = Helper.shuffleArray([
            1, 2, 3, 4
        ]);
        this._rightAnswer = parseInt(data["exercise-understood-exercise-correct"]);

        await Helper.asyncForEach(this._answerIndexes, async (index, i) => {
            (await this.findBy(".exercise-understood-exercise-answer-" + (i + 1), null, true))
                .appendChild(document.createTextNode(data["exercise-understood-exercise-answer-" + index]));
        });
    }

    async setState(state) {
        if (state["exercise-understood-exercise-selected"]) {
            let elem = (await this.findBy("[name='exercise-understood-exercise-correct'][value='"
                + this._answerIndexes.indexOf(state["exercise-understood-exercise-selected"])
                + "']", null, true));
            elem.checked = true;
        }
    }
}