import {MBBModel} from "./MBBModel";
import {BaseDatabase} from "cordova-sites-database/dist/cordova-sites-database";
import {Exercise} from "./Exercise";

export class Course extends MBBModel {

    name: string;
    icon: string;
    activated: boolean;
    progress: number;
    exercises: Exercise[];

    constructor() {
        super();
        this.name = null;
        this.icon = null;
        this.activated = false;
        this.progress = 0;
        this.exercises = undefined;
    }

    static getColumnDefinitions() {
        let columns = super.getColumnDefinitions();
        columns["name"] = BaseDatabase.TYPES.STRING;
        columns["activated"] = BaseDatabase.TYPES.BOOLEAN;
        columns["progress"] = BaseDatabase.TYPES.FLOAT;
        columns["icon"] = BaseDatabase.TYPES.MEDIUMTEXT;
        return columns;
    }

    static getRelationDefinitions() {
        let relations = super.getRelationDefinitions();
        relations["exercises"] = {
            target: Exercise.getSchemaName(),
            type: "one-to-many",
            // cascade: true,
            inverseSide: "course"
        };
        return relations;
    }
}

BaseDatabase.addModel(Course);