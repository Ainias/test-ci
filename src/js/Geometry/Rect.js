import {Point} from "./Point";
import {Helper} from "js-helper/dist/shared/Helper";

export class Rect {

    constructor(p1, p2) {
        this.p1 = new Point();
        this.p2 = new Point();
        this.set(p1, p2);
    }

    set(p1, p2) {
        if (Helper.isNotNull(p1)) {
            this.p1.set(
                Math.min(p1.x, p2.x),
                Math.min(p1.y, p2.y),
                Math.min(p1.z, p2.z),
            );
        }
        if (Helper.isNotNull(p2)) {
            this.p2.set(
                Math.max(p1.x, p2.x),
                Math.max(p1.y, p2.y),
                Math.max(p1.z, p2.z),
            );
        }
    }

    getAdjacentFace(other) {
        if (this.isOverlapping(other)) {
            return null;
        }

        let face = other.getOverlappingRect(new Rect(this.p1.copy(), this.p2.copy().setX(this.p1.x)));
        if (face.getYZArea() > 0) {
            return 0;
        }

        face = other.getOverlappingRect(new Rect(this.p1.copy().setZ(this.p2.z), this.p2.copy()));
        if (face.getXYArea() > 0) {
            return 1;
        }

        face = other.getOverlappingRect(new Rect(this.p1.copy().setX(this.p2.x), this.p2.copy()));
        if (face.getYZArea() > 0) {
            return 2;
        }

        face = other.getOverlappingRect(new Rect(this.p1.copy(), this.p2.copy().setZ(this.p1.z)));
        if (face.getXYArea() > 0) {
            return 3;
        }

        face = other.getOverlappingRect(new Rect(this.p1.copy(), this.p2.copy().setY(this.p1.y)));
        if (face.getXZArea() > 0) {
            return 4;
        }

        face = other.getOverlappingRect(new Rect(this.p1.copy().setY(this.p2.y), this.p2.copy()));
        if (face.getXZArea() > 0) {
            return 5;
        }
        return null;
    }


    isInside(point) {
        return (((point.x >= this.p1.x && point.x <= this.p2.x) || (point.x <= this.p1.x && point.x >= this.p2.x))
            && ((point.y >= this.p1.y && point.y <= this.p2.y) || (point.y <= this.p1.y && point.y >= this.p2.y))
            && ((point.z >= this.p1.z && point.z <= this.p2.z) || (point.z <= this.p1.z && point.z >= this.p2.z)));
    }

    isOverlapping(other) {
        return !((this.p2.x <= other.p1.x) ||
            (this.p1.x >= other.p2.x) ||

            (this.p2.y <= other.p1.y) ||
            (this.p1.y >= other.p2.y) ||

            (this.p2.z <= other.p1.z) ||
            (this.p1.z >= other.p2.z));
    }

    getOverlappingRect(other) {
        if (!this.isOverlappingStrong(other)) {
            return new Rect();
        }
        return new Rect(Point.max(this.p1, other.p1), Point.min(this.p2, other.p2));
    }

    getVolume() {
        let lengths = this.p2.copy().substract(this.p1);
        return lengths.x * lengths.y * lengths.z;
    }

    getXYArea() {
        let lengths = this.p2.copy().substract(this.p1);
        return lengths.x * lengths.y;
    }

    getXZArea() {
        let lengths = this.p2.copy().substract(this.p1);
        return lengths.x * lengths.z;
    }

    getYZArea() {
        let lengths = this.p2.copy().substract(this.p1);
        return lengths.y * lengths.z;
    }

    isOverlappingStrong(other) {
        return !((this.p2.x < other.p1.x) ||
            (this.p1.x > other.p2.x) ||

            (this.p2.y < other.p1.y) ||
            (this.p1.y > other.p2.y) ||

            (this.p2.z < other.p1.z) ||
            (this.p1.z > other.p2.z));
    }

    equals(other) {
        return this.p1.equals(other.p1)
            && this.p2.equals(other.p2);
    }

    multiply(point){
        this.p1.multiply(point);
        this.p2.multiply(point);
        return this;
    }
}