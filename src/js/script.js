import translationGerman from '../translations/de.json';
import translationEn from '../translations/en.json';
import {
    App,
    MenuAction,
    Submenu,
    Translator,
    NavbarFragment,
    DataManager,
    Toast
} from "cordova-sites/dist/client";
import {ListCoursesSite} from "./Sites/ListCoursesSite";
import {SyncJob} from "cordova-sites-easy-sync/dist/client";

//Importing TextSpeaker, so callback will be executed
import './TextSpeaker/TextSpeaker';
import "cordova-sites-user-management/dist/client/js/translationInit"
import "cordova-sites/dist/client/js/translationInit"

//Import flags for translations (do not delete)
import "../img/flag_german.svg";
import "../img/flag_usa.svg";
import "../img/logo.png";
import * as typeorm from "typeorm";

//Importing of Models
import {UserManager} from "cordova-sites-user-management/dist/client";
import {Exercise} from "../../model/Exercise";
import {Course} from "../../model/Course.ts";
import {Definition} from "../../model/Definition.ts";
import {ExerciseProgress} from "../../model/ExerciseProgress";

import {BaseDatabase} from "cordova-sites-database/dist/cordova-sites-database";
import {DeleteUserManagement1000000000000} from "cordova-sites-user-management/dist/shared/migrations/DeleteUserManagement";
import {SetupUserManagement1000000001000} from "cordova-sites-user-management/dist/shared/migrations/SetupUserManagement";
import {Setup1000000002000} from "../../model/migrations/shared/Setup";
import {EasySyncClientDb, SetupEasySync1000000000500} from "cordova-sites-easy-sync/dist/client";
import {DefinitionImageNullable1000000005000} from "../../model/migrations/shared/DefinitionImageNullable";
import {ErrorAction} from "cordova-sites/dist/client";
import {ExerciseProgressToPartialModel1000000006000} from "../../model/migrations/client/ExerciseProgressToPartialModel";
import {Brackets, In} from "typeorm";
import {Helper} from "js-helper/dist/shared/Helper";
import {User} from "cordova-sites-user-management/dist/shared/v1/model/User";
import {WrongAnswer} from "../../model/WrongAnswer";
import {AddWikiEntry1000000007000} from "../../model/migrations/shared/AddWikiEntry";
import {WikiEntry} from "../../model/WikiEntry";
import CKEditor from "@ckeditor/ckeditor5-build-classic";
import {Calculator} from "./Dialog/Calculator";
import {UserMenuAction} from "cordova-sites-user-management/dist/client/js/MenuAction/UserMenuAction";
import {ChangeUserSite} from "cordova-sites-user-management/dist/client/js/Site/ChangeUserSite";
import {ClearDatabaseJob} from "./ClearDatabase/ClearDatabaseJob";

window["JSObject"] = Object;
window["CKEditor"] = CKEditor;

//Disable Google Analytics for VideoJS
window["HELP_IMPROVE_VIDEOJS"] = false;

App.addInitialization(async () => {
    Translator.init({
        translations: {
            "de": translationGerman,
            "en": translationEn
        },
        fallbackLanguage: "de",
        // markTranslations: true,
        markUntranslatedTranslations: true,
    });

    let submenuTest = new Submenu("sprache", null, 1001);

    submenuTest.addAction(new MenuAction("de", () => {
        Translator.getInstance().setLanguage("de");
    }));
    submenuTest.addAction(new MenuAction("en", () => {
        Translator.getInstance().setLanguage("en");
    }));
    NavbarFragment.defaultActions.push(submenuTest.getParentAction());

    // NavbarFragment.defaultActions.push(new UserMenuAction("changeUser", "admin", () => {
    //     app.startSite(ChangeUserSite, {id: 1});
    // }));

    NavbarFragment.defaultActions.push(ErrorAction.getInstance());
    NavbarFragment.defaultActions.push(new MenuAction("calculator", () => {
        new Calculator().show();
    }));

    DataManager.onlineCallback = isOnline => {
        let error = "No connection to server. May not be online";
        if (!isOnline) {
            // new Toast("not online!").show();
            ErrorAction.addError(error)
        } else {
            ErrorAction.removeError(error);
        }
    };

    //login in Background
    await UserManager.getInstance().getMe().catch(e => console.error(e));
    let syncJob = new SyncJob();
    await syncJob.syncInBackgroundIfDataExists([Course, Exercise, WikiEntry, UserManager.syncParamFor(Definition), UserManager.userSyncParam()]).catch(e => console.error(e));

    //Workaround Bug wenn Elemente zur richtigen Zeit geladen werden, sind beziehungen nicht vorhanden
    // await syncJob.getSyncPromise();

    UserManager.getInstance().addLoginChangeCallback(async (isLoggedIn) => {
        if (isLoggedIn) {
            await new SyncJob().sync([UserManager.syncParamFor(Definition), UserManager.userSyncParam()]).catch(e => console.error(e));
        }
    });

    //Send data to server
    new Promise(async resolve => {
        let user = new User();
        user.id = UserManager.getInstance().getUserData().id;
        if (Helper.isNotNull(user.id)) {
            let queryBuilder = await EasySyncClientDb.getInstance().createQueryBuilder(ExerciseProgress);
            queryBuilder = queryBuilder
                .innerJoinAndSelect("ExerciseProgress.element", "element")
                .andWhere("ExerciseProgress.id IS NULL").andWhere(new Brackets(qb => {
                    qb.where("ExerciseProgress.isDone = 1").orWhere("ExerciseProgress.clientId NOT IN " +
                        qb.subQuery().select("MAX(exerciseProgress2.clientId)").from("ExerciseProgress", "exerciseProgress2").groupBy("elementId").getQuery()
                    );
                }));

            let loadedProgresses = await queryBuilder.getMany();

            let clientIds = [];

            loadedProgresses.forEach(progress => {
                progress.user = user;
                clientIds.push(progress.clientId)
            });
            let savePromise = ExerciseProgress.saveMany(loadedProgresses, false);
            let wrongAnswers = await WrongAnswer.find({exerciseProgress: {clientId: In(clientIds)}});
            wrongAnswers.forEach(wrongAnswer => wrongAnswer.user = user);
            await WrongAnswer.saveMany(wrongAnswers, false);
            await savePromise;
        }

        resolve();
    });
});

DataManager._basePath = __HOST_ADDRESS__;


let synchronizeDb = false;
if (synchronizeDb !== true) {
}
Object.assign(BaseDatabase.CONNECTION_OPTIONS, {
    logging: ["error",],
    synchronize: false,
    migrationsRun: true,
    migrations: [
        DeleteUserManagement1000000000000,
        SetupEasySync1000000000500,
        SetupUserManagement1000000001000,
        Setup1000000002000,
        DefinitionImageNullable1000000005000,
        ExerciseProgressToPartialModel1000000006000,
        AddWikiEntry1000000007000,
    ]
});

EasySyncClientDb.errorListener = async (e) => {
    console.error(e);
    debugger;
    await ClearDatabaseJob.doJob();
    //work with timeout since saving of db only occurs after 150ms in browser
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(window.location.reload(true));
        }, 200);
    });
};

let app = new App();
app.start(ListCoursesSite);
app.ready(() => {
    console.log("initialisation over", new Date());

    //For testing purposes
    window["queryDb"] = async (sql) => {
        let res = await EasySyncClientDb.getInstance().rawQuery(sql);
        console.log(res);
        return res;
    };
});