import {MBBModel} from "./MBBModel";
import {BaseDatabase} from "cordova-sites-database/dist/cordova-sites-database";
import {Course} from "./Course";

export class Exercise extends MBBModel {

    generatingData: Object;
    course: Course;
    progress: number;
    name: string;
    isGenerating: boolean;
    elementType: string;

    constructor() {
        super();

        this.generatingData = {};
        this.course = null;
        this.progress = null;
        this.name = null;
        this.isGenerating = false;

        this.elementType = null;
    }

    setCourse(course){
        this.course = course;
    }

    setGeneratingData(generatingData){
        this.generatingData = generatingData;
    }

    setElementType(type) {
        this.elementType = type;
    }

    getElementType() {
        return this.elementType;
    }

    setName(name) {
        this.name = name;
    }

    getName() {
        return this.name;
    }

    static getColumnDefinitions() {
        let columns = super.getColumnDefinitions();
        columns["elementType"] = {type: BaseDatabase.TYPES.STRING};
        columns["generatingData"] = {
            type: BaseDatabase.TYPES.MY_JSON,
            nullable: true
        };
        columns["name"] = BaseDatabase.TYPES.STRING;
        columns["isGenerating"] = {
            type: BaseDatabase.TYPES.BOOLEAN,
            default: false
        };
        return columns;
    }

    static getRelationDefinitions() {
        let relations = super.getRelationDefinitions();
        relations["course"] = {
            target: Course.getSchemaName(),
            type: "many-to-one",
            joinColumn: true,
            inverseSide: "exercises"
        };
        return relations;
    }
}

BaseDatabase.addModel(Exercise);