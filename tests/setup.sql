-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: mbb
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Dumping data for table `access`
--

LOCK TABLES `access` WRITE;
/*!40000 ALTER TABLE `access`
    DISABLE KEYS */;
INSERT INTO `access`
VALUES (1, '2019-06-04 16:51:17', '2019-06-04 16:51:17', 2, 0, 'default', 'everyone is allowed to do this!'),
       (2, '2019-06-04 16:51:17', '2019-06-04 16:51:17', 2, 0, 'offline', 'does not has internet access!'),
       (3, '2019-06-04 16:51:17', '2019-06-04 16:51:17', 2, 0, 'online', 'has internet access'),
       (4, '2019-06-04 16:51:17', '2019-06-04 16:51:17', 2, 0, 'loggedOut', 'for users, that are not logged in'),
       (5, '2019-06-04 16:51:17', '2019-06-04 16:51:17', 2, 0, 'loggedIn', 'for users, that are logged in'),
       (6, '2019-06-04 16:51:17', '2019-06-04 16:51:17', 2, 0, 'admin', 'Access for admins');
/*!40000 ALTER TABLE `access`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course`
    DISABLE KEYS */;
INSERT INTO `course`
VALUES (1, '2019-06-04 16:51:19', '2019-06-04 16:51:19', 2, 0, 'Mauerwerksbau', 1, 0,
        'https://upload.wikimedia.org/wikipedia/commons/3/36/Stadtpfarrkirche_Sankt_Peter.jpg');
INSERT INTO `course`
VALUES (5, '2019-06-04 16:51:24', '2019-07-12 12:22:39', 4, 0, 'Stahlbetonbau', 1, 0,
        'https://upload.wikimedia.org/wikipedia/commons/3/36/Stadtpfarrkirche_Sankt_Peter.jpg');
/*!40000 ALTER TABLE `course`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `definition`
--

LOCK TABLES `definition` WRITE;
/*!40000 ALTER TABLE `definition`
    DISABLE KEYS */;
INSERT INTO `definition`
VALUES (1, '2019-07-24 16:41:44', '2019-07-24 16:41:53', 1, 0, 1, 'sf', '[\"bf\"]', 'svb', NULL, NULL),
       (2, '2019-07-24 17:35:57', '2019-07-24 17:36:09', 1, 0, 1, 'admin', '[\"ad\"]', 'da', NULL, NULL),
       (3, '2019-07-24 19:10:35', '2019-07-24 19:10:42', 1, 0, 1, 'jlasd', '[\"asdfbh\"]', 'bwe', NULL, NULL),
       (4, '2019-07-24 19:10:35', '2019-07-24 19:13:39', 1, 0, 1, 'drh', '[\"asdfbh\"]', 'bwe', NULL, NULL),
       (5, '2019-07-24 19:10:35', '2019-07-24 19:14:30', 1, 0, 1, 'lkhg', '[\"asdfbh\"]', 'bwe', NULL, NULL),
       (6, '2019-07-24 19:40:41', '2019-07-24 19:42:02', 1, 0, 1, 'juhuch', '[\"yey...\"]', 'mhhh', NULL, NULL),
       (7, '2019-07-24 19:43:58', '2019-07-24 19:44:51', 1, 0, 1, 'aadmin', '[\"aadmin\"]', 'beschreibung', NULL, NULL),
       (8, '2019-07-24 19:43:58', '2019-07-24 19:48:44', 1, 0, 1, 'aadmin2', '[\"aadmin\"]', 'beschreibung', NULL,
        NULL),
       (9, '2019-07-24 19:43:58', '2019-07-24 19:50:57', 1, 0, 1, 'aadmin22', '[\"aadmin\"]', 'beschreibung', NULL,
        NULL),
       (10, '2019-07-24 19:54:05', '2019-07-24 19:54:45', 1, 0, 1, 'aaadmin', '[\"admin\"]', 'admin', NULL, NULL),
       (11, '2019-07-24 20:04:16', '2019-07-24 20:04:30', 2, 0, 1, 'aaaaaaaa', '[\"aaa\"]', 'aaaa', NULL, NULL),
       (12, '2019-08-15 13:16:53', '2019-08-15 13:17:07', 2, 0, 1, 'test', '[\"me\"]', 'hhhard', NULL, NULL),
       (13, '2019-08-15 13:17:23', '2019-08-15 13:17:34', 2, 0, NULL, 'jou', '[\"forall\"]', 'bes', NULL, NULL);
/*!40000 ALTER TABLE `definition`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `exercise`
--

LOCK TABLES `exercise` WRITE;
/*!40000 ALTER TABLE `exercise`
    DISABLE KEYS */;
INSERT INTO `exercise`
VALUES (25, '2019-07-24 15:56:47', '2019-07-24 15:56:49', 2, 0, 'element-wall-length-exercise', 'My ex', 0,
        '{\"exercise-name\":\"My ex\",\"num-whole-stone\":1,\"num-half-stone\":7,\"num-quarter-stone\":5,\"num-three-quarter-stone\":0,\"num-stones-sideways\":2,\"exercise-description\":\"des\",\"exercise-image-description\":\"img\",\"exercise-image\":\"https://upload.wikimedia.org/wikipedia/commons/3/36/Stadtpfarrkirche_Sankt_Peter.jpg\"}',
        1);
/*!40000 ALTER TABLE `exercise`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `exercise_progress`
--

LOCK TABLES `exercise_progress` WRITE;
/*!40000 ALTER TABLE `exercise_progress`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `exercise_progress`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role`
    DISABLE KEYS */;
INSERT INTO `role`
VALUES (1, '2019-06-04 16:51:17', '2019-06-04 16:51:17', 2, 0, 'offlineRole', 'role for user that are offline'),
       (2, '2019-06-04 16:51:17', '2019-06-04 16:51:17', 2, 0, 'onlineRole', 'role for user that are online'),
       (3, '2019-06-04 16:51:17', '2019-06-04 16:51:17', 2, 0, 'visitorRole',
        'role for user that are online, but not logged in'),
       (4, '2019-06-04 16:51:17', '2019-06-04 16:51:17', 2, 0, 'memberRole',
        'role for user that are online and logged in'),
       (5, '2019-06-04 16:51:18', '2019-06-04 16:51:18', 2, 0, 'Admin',
        'Role for Admins (online, logged in and admin)');
/*!40000 ALTER TABLE `role`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `roleAccess`
--

LOCK TABLES `roleAccess` WRITE;
/*!40000 ALTER TABLE `roleAccess`
    DISABLE KEYS */;
INSERT INTO `roleAccess`
VALUES (1, 1),
       (1, 2),
       (2, 1),
       (2, 3),
       (3, 4),
       (4, 5),
       (5, 6);
/*!40000 ALTER TABLE `roleAccess`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `roleChildren`
--

LOCK TABLES `roleChildren` WRITE;
/*!40000 ALTER TABLE `roleChildren`
    DISABLE KEYS */;
INSERT INTO `roleChildren`
VALUES (3, 2),
       (4, 2),
       (5, 4);
/*!40000 ALTER TABLE `roleChildren`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user`
    DISABLE KEYS */;

INSERT INTO `user`
VALUES (1, '2019-12-19 13:04:56', '2019-12-19 13:04:56', 1, 0, 'testuser', 'testuser@mbb.de',
        '9d6215b360002f33d70399bbe7bf20d87140db83e7d12f6aaff737fb404265bbf80395e6d9caed58922e45a6d738195f2781043301a27ecfa6dddbaaf37abe44',
        1, 0, 'ff5622155d7f'),
       (2, '2019-06-04 16:51:18', '2019-06-04 16:51:24', 3, 0, 'admin', 'admin@mbb.de',
        'be9ad32628ff3eaded964bd41ef30c3f4e63d5c9ef35a4fb96f0f071611c272d8200109315910b337837f6971dbd5d72a3ca8f2d1f4f822767e8ad1bae35c73b',
        1, 0, 'f2ffcb0d0ba8');
/*!40000 ALTER TABLE `user`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `userRole`
--

LOCK TABLES `userRole` WRITE;
/*!40000 ALTER TABLE `userRole`
    DISABLE KEYS */;
INSERT INTO `userRole`
VALUES (1, 4),
       (2, 5);
/*!40000 ALTER TABLE `userRole`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user_access`
--

LOCK TABLES `user_access` WRITE;
/*!40000 ALTER TABLE `user_access`
    DISABLE KEYS */;
INSERT INTO `user_access`
VALUES (1, 1, 1),
       (2, 1, 3),
       (3, 1, 5),
       (4, 2, 1),
       (5, 2, 3),
       (6, 2, 5),
       (7, 2, 6);
/*!40000 ALTER TABLE `user_access`
    ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations`
    DISABLE KEYS */;
INSERT INTO `migrations`
VALUES (43, 900000000000, 'DeleteData0900000000000'),
       (44, 1000000000000, 'DeleteUserManagement1000000000000'),
       (45, 1000000001000, 'SetupUserManagement1000000001000'),
       (46, 1000000002000, 'Setup1000000002000'),
       (47, 1000000005000, 'Data1000000005000'),
       (48, 1000000005000, 'DefinitionImageNullable1000000005000');
/*!40000 ALTER TABLE `migrations`
    ENABLE KEYS */;
UNLOCK TABLES;


/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2019-08-15 13:30:26
