import {MigrationInterface, QueryRunner, Table} from "typeorm";
import {BaseDatabase} from "cordova-sites-database/dist/BaseDatabase";

export class ExerciseProgressToPartialModel1000000006000 implements MigrationInterface {

    _isServer(): boolean {
        return (typeof document !== "object")
    }

    async up(queryRunner: QueryRunner): Promise<any> {
        await this._changeExerciseProgress(queryRunner);
        await this._changeWrongAnswer(queryRunner);
    }


    async _changeExerciseProgress(queryRunner: QueryRunner) {

       await queryRunner.dropTable("exercise_progress");

        let definition = new Table({
            name: "exercise_progress",
            columns: [
                {
                    name: "id",
                    isPrimary: false,
                    type: BaseDatabase.TYPES.INTEGER,
                    isGenerated: false,
                    isNullable: true,
                },
                {
                    name: "clientId",
                    isPrimary: true,
                    type: "integer",
                    isGenerated: true,
                    generationStrategy: "increment"
                },
                {
                    name: "createdAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "updatedAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "version",
                    type: BaseDatabase.TYPES.INTEGER,
                },
                {
                    name: "deleted",
                    type: BaseDatabase.TYPES.BOOLEAN,
                },
                {
                    name: "userId",
                    type: BaseDatabase.TYPES.INTEGER,
                    isNullable: true
                },
                {
                    name: "state",
                    type: (this._isServer() ? BaseDatabase.TYPES.MEDIUMTEXT : BaseDatabase.TYPES.TEXT),
                },
                {
                    name: "timeNeeded",
                    type: BaseDatabase.TYPES.INTEGER
                },
                {
                    name: "runNumber",
                    type: BaseDatabase.TYPES.INTEGER,
                },
                {
                    name: "data",
                    type: (this._isServer() ? BaseDatabase.TYPES.MEDIUMTEXT : BaseDatabase.TYPES.TEXT),
                    isNullable: true
                },
                {
                    name: "isDone",
                    type: BaseDatabase.TYPES.BOOLEAN,
                },
                {
                    name: "elementId",
                    type: BaseDatabase.TYPES.INTEGER,
                    isNullable: true
                },
            ],
            indices: [
                {
                    name: "IDX_exercise_progress_userId",
                    columnNames: ["userId"]
                },
                {
                    name: "IDX_exercise_progress_elementId",
                    columnNames: ["elementId"]
                },
            ],
            foreignKeys: [
                {
                    name: "FK_exercise_progress_userId",
                    columnNames: ["userId"],
                    referencedTableName: "user",
                    referencedColumnNames: ["id"],
                },
                {
                    name: "FK_exercise_progress_elementId",
                    columnNames: ["elementId"],
                    referencedTableName: "exercise",
                    referencedColumnNames: ["id"],
                },
            ]
        });
        return await queryRunner.createTable(definition, true)
    }

    async _changeWrongAnswer(queryRunner: QueryRunner) {

        await queryRunner.dropTable("wrong_answer");

        let wrongAnswer = new Table({
            name: "wrong_answer",
            columns: [
                {
                    name: "id",
                    isPrimary: false,
                    type: BaseDatabase.TYPES.INTEGER,
                    isGenerated: false,
                    isNullable: true,
                },
                {
                    name: "clientId",
                    isPrimary: true,
                    type: "integer",
                    isGenerated: true,
                    generationStrategy: "increment"
                },
                {
                    name: "createdAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "updatedAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "version",
                    type: BaseDatabase.TYPES.INTEGER,
                },
                {
                    name: "deleted",
                    type: BaseDatabase.TYPES.BOOLEAN,
                },
                {
                    name: "userId",
                    type: BaseDatabase.TYPES.INTEGER,
                    isNullable: true
                },
                {
                    name: "field",
                    type: BaseDatabase.TYPES.STRING,
                },
                {
                    name: "given",
                    type: BaseDatabase.TYPES.STRING,
                    isNullable: true,
                },
                {
                    name: "expected",
                    type: BaseDatabase.TYPES.STRING,
                },
                {
                    name: "occurredAt",
                    type: BaseDatabase.TYPES.DATE
                },
                {
                    name: "exerciseProgressClientId",
                    type: BaseDatabase.TYPES.INTEGER,
                    isNullable: true
                },
            ],
            indices: [
                {
                    name: "IDX_wrong_answer_userId",
                    columnNames: ["userId"]
                },
                {
                    name: "IDX_wrong_answer_exerciseProgressClientId",
                    columnNames: ["exerciseProgressClientId"]
                },
            ],
            foreignKeys: [
                {
                    name: "FK_wrong_answer_userId",
                    columnNames: ["userId"],
                    referencedTableName: "user",
                    referencedColumnNames: ["id"],
                },
                {
                    name: "FK_wrong_answer_exerciseProgressClientId",
                    columnNames: ["exerciseProgressClientId"],
                    referencedTableName: "exercise_progress",
                    referencedColumnNames: ["clientId"],
                },
            ]
        });
        return await queryRunner.createTable(wrongAnswer, true)
    }

    down(queryRunner: QueryRunner): Promise<any> {
        return undefined;
    }
}