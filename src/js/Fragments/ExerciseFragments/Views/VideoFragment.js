import view from "../../../../html/Fragments/ExerciseFragments/Exercises/videoFragment.html";
import {Helper, Translator, TabFragment} from "cordova-sites/dist/client";
import videojs from "video.js";

export class VideoFragment extends TabFragment {

    constructor(site) {
        super(site, view);
    }

    async onViewLoaded() {
        let res = super.onViewLoaded();
        this._tabViewPromise.then(() => {
            this._videoElement = this.findBy(".element-video");
            this._videoAlternativeTextElement = this.findBy(".element-video-alternative-text");
            this._videoFulltextElement = this.findBy(".element-video-fulltext");

            this._updateVideoView();
        });
        return res;
    }

    setElement(element) {
        this._element = element;
        this._updateVideoView();
    }

    goBack() {
        return this.getSite().goBack();
    }

    _updateVideoView() {
        if (Helper.isNotNull(this._videoElement) && Helper.isNotNull(this._element)) {
            let data = this._element.getData();

            this._videoAlternativeTextElement.innerText = this._element.getData()["element-video-alternative-text"];
            this._videoFulltextElement.innerText = this._element.getData()["element-video-fulltext"];

            let videoSources = [];
            let audioTracks = [];

            if (data["element-video-src"]) {
                audioTracks.push(new videojs.AudioTrack({
                    id: "my-id" + 1,
                    label: Translator.getInstance().translate("german"),
                    kind: "main",
                    language: "de"
                }));
                videoSources.push({
                    src: data["element-video-src"],
                });
            }
            if (data["element-video-audiodescripted-src"]) {
                audioTracks.push(new videojs.AudioTrack({
                    id: "my-id" + 2,
                    label: Translator.getInstance().translate("audiodescription"),
                    kind: "description",
                    language: "de"
                }));
                videoSources.push({
                    src: data["element-video-audiodescripted-src"],
                });
            }

            let videoPlayer = videojs(this._videoElement, {
                controls: true
            });
            let audioTrackList = videoPlayer.audioTracks();

            videoSources.forEach((track, i) => {
                //extract type from string
                track.type = track.src.split(";")[0].split(":")[1];
                if (i === 0) {
                    audioTracks[i].enabled = true;
                    videoPlayer.src(track);
                }
                audioTrackList.addTrack(audioTracks[i]);
            });

            audioTrackList.addEventListener('change', async () => {
                for (let i = 0; i < audioTrackList.length; i++) {
                    let track = audioTrackList[i];
                    if (track.enabled) {
                        let currentTime = videoPlayer.currentTime();
                        let isPaused = videoPlayer.paused();

                        videoPlayer.pause();
                        videoPlayer.src(videoSources[i]);

                        await videoPlayer.play();
                        videoPlayer.currentTime(currentTime);
                        if (isPaused) {
                            videoPlayer.pause();
                        }
                    }
                }
            });

            videoPlayer.addRemoteTextTrack({
                src: data["element-video-subtitles"],
                label: "german",
                lang: "de",
                kind: "captions"
            });
        }
    }
}