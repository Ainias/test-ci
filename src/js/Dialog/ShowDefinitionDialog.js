import view from "../../html/Dialog/showDefinition.html";
import {Lexicon} from "../Lexicon";
import {Dialog, Helper} from "cordova-sites/dist/client";

export class ShowDefinitionDialog extends Dialog{

    constructor(definition) {
        super(view, "");
        this.setTranslatable(false);
        this.setAdditionalClasses("no-title");

        this._definition = definition;
    }

    async setContent(content) {
        let res = super.setContent(content);
        res  = await res ;

        content = this._content;
        content.querySelector(".key").appendChild(document.createTextNode(this._definition.getKey()));
        content.querySelector(".keywords").appendChild(document.createTextNode(this._definition.getKeywords().join(", ")));
        content.querySelector(".description").appendChild(await Lexicon.getInstance().prepareText(this._definition.getDescription(), this._definition.getKey()));

        let img = this._definition.getImage();
        if (Helper.isNotNull(img)){
            content.querySelector(".image-container").classList.remove("hidden");
            content.querySelector(".image").setAttribute("src", img);
            content.querySelector(".image-description").appendChild(await Lexicon.getInstance().prepareText(this._definition.getImageDescription(), this._definition.getKey()));
        }

        return res;
    }
}