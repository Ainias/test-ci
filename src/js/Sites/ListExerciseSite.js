import view from "../../html/sites/listExercisesSite.html";
import {ShowExerciseSite} from "./ShowExerciseSite";
import {LexiconSite} from "./LexiconSite";
import {SelectExerciseDialog} from "../Dialog/SelectExerciseDialog";
import {Helper, MenuAction, MenuSite} from "cordova-sites/dist/client";
import {UserMenuAction} from "cordova-sites-user-management/dist/client";
import {Course} from "../../../model/Course.ts"
import {Exercise} from "../../../model/Exercise.ts";
import {EditExerciseSite} from "./EditExerciseSite";
import {EditStorybookSite} from "./EditStorybookSite";

export class ListExerciseSite extends MenuSite {
    constructor(siteManager) {
        super(siteManager, view);
    }

    async onConstruct(constructParameters) {
        let res = super.onConstruct(constructParameters);
        this._courseId = constructParameters["courseId"];
        return res;
    }

    async onViewLoaded() {

        this._exerciseTemplate = this.findBy("#article-template");
        this._exerciseTemplate.remove();
        this._exerciseTemplate.removeAttribute("id");

        this._exerciseContainer = this.findBy("#article-container");

        return super.onViewLoaded();
    }

    async _updateExercises() {
        let exercises = [];
        if (Helper.isNotNull(this._courseId)) {
            let course = await Course.findById(this._courseId, Course.getRelations());
            exercises = course.exercises;
        } else {
            exercises = await Exercise.find(undefined, {"id": "ASC"});
        }
        Helper.removeAllChildren(this._exerciseContainer);
        exercises.forEach(exercise => this._addExercise(exercise));
    }

    onCreateMenu(navbar) {
        navbar.addAction(new UserMenuAction("edit-article-site", "admin", async () => {
            let type = await new SelectExerciseDialog().show();
            if (typeof type === "string") {
                if (type === "storybook") {
                    this.startSite(EditStorybookSite, {courseId: this._courseId});
                } else {
                    this.startSite(EditExerciseSite, {type: type, course: this._courseId});
                }
            }
        }));

        navbar.addAction(new MenuAction("lexicon-site", () => {
            this.startSite(LexiconSite);
        }));

        // navbar.addAction(new UserMenuAction("generate exercises", "admin", async () => {
        //     let type = await new GenerateExerciseDialog().show();
        //     if (typeof type === "string") {
        //         this.startSite(GeneratorSite, {
        //             "courseId": this._courseId,
        //             "exercise": type
        //         });
        //     }
        // }))
    }

    async onStart(pauseArguments) {
        await super.onStart(pauseArguments);
        await this._updateExercises();
    }

    /**
     *
     * @param {Exercise} exercise
     * @private
     */
    _addExercise(exercise) {
        let exerciseElement = this._exerciseTemplate.cloneNode(true);
        let name = exerciseElement.querySelector(".name");
        name.appendChild(document.createTextNode(exercise.getName() + (exercise.isGenerating ? " (generated)" : "")));
        exerciseElement.addEventListener("click", () => {
            this.startSite(ShowExerciseSite, {exerciseId: exercise.getId()})
        });

        this._exerciseContainer.appendChild(exerciseElement);
    }
}