import {AbstractFragment} from "cordova-sites/dist/client/js/Context/AbstractFragment";

import view from "../../../html/Fragments/statistic/wrongAnswerPerDayFragment.html";
import {DateHelper} from "js-helper/dist/shared/DateHelper";
import {Helper} from "js-helper/dist/shared";

export class WrongAnswerPerDayFragment extends AbstractFragment {

    constructor(site) {
        super(site, view);
        this._timeDiagram = null;
    }

    async setStatistic(statistic) {
        await this._viewLoadedPromise;

        let canvas = this.findBy("#wrong-answer-time-diagram");

        let timeCounts = {};

        statistic["data"].forEach(dataset => {
            let dateString = DateHelper.strftime("%Y-%m-%d", new Date(dataset["wrongAnswerOccurredAt"]));
            if (!Helper.isSet(timeCounts, dateString)) {
                timeCounts[dateString] = 0;
            }
            timeCounts[dateString]++;
        });

        let days = Object.keys(timeCounts).sort();
        if (days.length > 0) {
            let startDay = new Date(days[0]);
            let endDay = new Date(days[days.length - 1]);
            while (startDay.getTime() < endDay.getTime()) {
                startDay.setDate(startDay.getDate() + 1);
                let dayString = DateHelper.strftime("%Y-%m-%d", startDay);
                if (!Helper.isSet(timeCounts[dayString])) {
                    timeCounts[dayString] = 0;
                }
            }
        }

        let timeCountsDataArray = [];
        Object.keys(timeCounts).forEach(day => {
            timeCountsDataArray.push([day, timeCounts[day]]);
        });

        timeCountsDataArray = timeCountsDataArray.sort((a, b) => {
            return a[0].localeCompare(b[0]);
        });

        let labels = [];
        let values = [];

        timeCountsDataArray.forEach(v => {
            labels.push(v[0]);
            values.push(v[1]);
        });

        let options = {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    data: values,
                    label: "# of Wrong Answers"
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
            }
        };
        if (Helper.isNotNull(this._timeDiagram)) {
            this._timeDiagram.destroy();
        }


        this._timeDiagram = new Chart(canvas.getContext("2d"), options);
    }
}