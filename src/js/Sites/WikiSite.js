import {MenuSite} from "cordova-sites/dist/client/js/Context/MenuSite";

import view from "../../html/sites/wikiEntrySite.html";
import {Helper} from "js-helper/dist/shared/Helper";
import {WikiEntry} from "../../../model/WikiEntry";
import {EditWikiEntrySite} from "./EditWikiEntrySite";
import {App} from "cordova-sites";
import {UserManager} from "cordova-sites-user-management/dist/client/js/UserManager";
import {ConfirmDialog} from "cordova-sites/dist/client/js/Dialog/ConfirmDialog";

export class WikiSite extends MenuSite {

    constructor(siteManager) {
        super(siteManager, view);
    }

    async onConstruct(constructParameters) {
        let res = super.onConstruct(constructParameters);

        let id = Helper.nonNull(constructParameters, {})["id"];
        this._wikiEntry = await WikiEntry.findById(id);
        if (Helper.isNull(this._wikiEntry)) {
            console.warn("no wiki-entry found! ", constructParameters);
            res.then(() => {
                this.finish();
            });
            return res;
        }
    }

    async onViewLoaded() {
        let res = super.onViewLoaded();

        let editButton = this.findBy("#edit");
        editButton.addEventListener("click", () => {
            this.startSite(EditWikiEntrySite, {id: this._wikiEntry.id})
        });

        this.findBy("#delete").addEventListener("click", async () => {
            let confirmDeleteDialog = new ConfirmDialog("delete the wiki entry?","delete wiki entry");
            if (await confirmDeleteDialog.show()){
                await this._wikiEntry.delete();
                this.finish();
            }
        });

        if (UserManager.getInstance().hasAccess("admin")){
           this.findBy("#admin-panel").classList.remove("hidden");
        }

        return res;
    }

    async onStart(pauseArguments) {
        super.onStart(pauseArguments);
        this.findBy("#name").innerText = this._wikiEntry.name;
        this.findBy("#description").innerHTML = this._wikiEntry.description;

    }
}

App.addInitialization((app) => {
    app.addDeepLink("wiki", WikiSite);
});