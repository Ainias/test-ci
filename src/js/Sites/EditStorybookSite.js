import {MenuSite} from "cordova-sites/dist/client/js/Context/MenuSite";

import view from "../../html/sites/editStorybookSite.html"
import {Exercise} from "../../../model/Exercise";
import {SaveStorybookDialog} from "../Dialog/SaveStorybookDialog";
import {Helper} from "cordova-sites/dist/client";
import {Course} from "../../../model/Course";
import {EditExerciseSite} from "./EditExerciseSite";

export class EditStorybookSite extends MenuSite {
    constructor(siteManager) {
        super(siteManager, view);
    }

    async onConstruct(constructParameters) {
        let res = super.onConstruct(constructParameters);

        this._exercise = null;
        this._courseId = null;
        if (Helper.isNotNull(constructParameters["course"])) {
            this._courseId = parseInt(constructParameters["course"]);
        }
        if (Helper.isNotNull(constructParameters["exercise"])) {
            this._exercise = await Exercise.findById(constructParameters["exercise"], Exercise.getRelations());
        }
        if (Helper.isNull(this._exercise)) {
            this._exercise = new Exercise();
        } else {
            if (this._exercise.elementType !== "storybook"){
                this.finishAndStartSite(EditExerciseSite, {course: this._courseId, exercise: this._exercise.id})
                return res;
            }
            this._courseId = this._exercise.course.id;
        }

        return res
    }

    onViewLoaded() {
        let res = super.onViewLoaded();

        let content = this.findBy("#editor");
        let editor = new GB.GBEditor(content, "de");
        editor.menubar.menu.AddMenuItem("Save", async () => {
            // let blob = new Blob([editor.Save()], { type: "application/json" });
            let res = await new SaveStorybookDialog(this._exercise.getName(), this._courseId).show();
            if (res){
                console.log("saving...", res);

                if (!this._exercise.course || this._exercise.course.id !== parseInt(res["course"])){
                    this._exercise.setCourse(await Course.findById(res["course"]));
                }

                this._exercise.setName(res["name"]);
                this._exercise.elementType = "storybook";
                this._exercise.isGenerating = false;
                this._exercise.generatingData = JSON.parse(editor.Save());

                await this._exercise.save();
                this.finish();
            }
        });

        //not a newly created exercise
        if (this._exercise.id){
            editor.Load(JSON.stringify(this._exercise.generatingData));
        }

        editor.menubar.menu.AddMenuItem("Reset", editor.Reset.bind(editor));

        return res;
    }
}