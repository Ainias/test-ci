const path = require("path");
const find = require("../lib/PromiseSelector");
const $ = find.one;

const functions = require("./functions");

describe("add exercises", () => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 30 * 1000;

    let baseUrl = null;
    beforeAll(async () => {
        if (browser.config.baseUrl.trim() !== "") {
            baseUrl = browser.config.baseUrl;
        } else {
            baseUrl = await browser.getUrl();
        }

        browser.setTimeout({
            implicit: 5000
        });
    });

    beforeEach(async function () {
        await browser.url(baseUrl);

        await browser.waitUntil(async () => {
            let element = $("#main-content");
            return await element.isDisplayed()
        });
        await $(".activated=Mauerwerksbau").click();
    });

    it("add wall length exercise", async function () {
        await functions.login(browser, "admin@mbb.de", "123456");

        await browser.pause(500);

        await $("#navbar-menu-visible").$("a=Neue Aufgabe").click();
        await $("#choose-container .choose-dialog-value-row[data-value=element-wall-length-exercise]").click();

        //Set data
        await $("[name=exercise-name]").setValue("my new exercise");
        await $("[name=number-num-whole-stone-lb]").setValue("1");
        await $("[name=number-num-half-stone-lb]").setValue("2");
        await $("[name=number-num-quarter-stone-lb]").setValue("3");
        await $("[name=number-num-stones-sideways-lb]").setValue("4");
        await $("[name=text-exercise-description]").setValue("this is my description");
        await $("[name=text-exercise-image-description]").setValue("this is my image description");

        await $("[name=image-exercise-image]").setValue(path.join(__dirname, "../img/cement.png"));

        await $("button[data-translation=save]").click();

        //wait until exercise is in DB
        expect(await $("#show-exercise-site").isDisplayed()).toBeTruthy();

        //check exercise values
        let exerciseData = await functions.queryDatabase(browser.config.mysqlConnection, "SELECT * FROM exercise ORDER BY id DESC LIMIT 1");
        exerciseData = exerciseData[0];
        expect(exerciseData["elementType"]).toEqual("element-wall-length-exercise");
        expect(exerciseData["name"]).toEqual("my new exercise");
        expect(exerciseData["isGenerating"]).toEqual(0);
        expect(exerciseData["generatingData"]).toEqual('{"exercise-name":"my new exercise","num-whole-stone":1,"num-three-quarter-stone":0,"num-half-stone":2,"num-quarter-stone":3,"num-stones-sideways":4,"exercise-description":"this is my description","exercise-image-description":"this is my image description","exercise-image":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAYAAAB5fY51AAAACXBIWXMAAAsSAAALEgHS3X78AAAR6UlEQVR4nO3dwXnbRhrG8Tf77F3uwNwKpFRgpoIoF+CAQ7gVmFuB6QpWrmDpAw/kZakKQlVgqYKlOhAr0B4wECGaAAhgBpgB/r/n0SOHNEhEiV/PfJz55pfX11cBQAj+1vcNAMClCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsICARXFy0/c9dOmX19fXvu8BQE1RnHyQtJT0u3no22a9mvd3R90gsIDAmFHVVtLHk6f+2KxX2x5uqTNMCYGARHGykPRDP4eVJC3NyGuw/t73DQCoFsXJROkU8FPJb7tSOvKaur+jfjDCAjwXxcmtpEeVh1XmUxQng61lUcMCPGWmdwtJnxtc/utmvXq0e0f9I7AAD5nC+lLSdcOXeJZ0s1mvXqzdlAeYEgKeMVO6nZqHlZQW5e+s3JBHGGEBnjiztsqGf27Wq6XF1+sVgQV4IIqTqdKwOrdcoY2D0qnh3vLr9oIpIdAzs7bqL9kPK+m41GEQGGEBPTFrq7ZqV6u61G+b9WrXwfs4xQgL6EFubVUXYSXzXsFjpTvQIVNYv5P0Z4dv+3UoyxsILKAjFtZW1fUs6XZIC0iZEgIdMGurfqi7sPqu9NPBwYSVxAgLcMpMAbe6bB+gDQdJ8yGtvcojsABHzNqqrdKlBV14UjoF3Hf0fp0jsAAHoji5U7NNy02NouMogQVY1PHaKimdAt4OYY3VJSi6A5ZEcTJTt2ur7iVNxhJWEiMsoDVHm5ar/GuzXg2uG0MVAgtooeRACFeeJM2GtlzhUkwJgYYqDoRw4buk6VjDSmKEBdTW09qq2dCP8LoEIyygBrNpea/uwupJ6Yr10YeVxAgLuEjLAyGa+rpZrxYdvp/3CCygQk+blmdjWq5wKaaEQAlLB0LUca90Crjr6P2CwggLOKOHtVUHSYsxrq2qg8ACTjg8EKLIqNdW1cGUEMhxfCDEOd808rVVdTDCAowoTrrcB8jaqgY4NQfQW5eF/3X0dg9KOywMos96lxhhAakPHb0Pa6taoIYFSDI1pG8O3+JZ0q+EVTsEFnC0UPqJnW3Z2ioK6y1RwwJyzKr2nez0YR/0gRB9YIQFKC26R3GSjYIWFl4y27S8tPBaMBhhYfRMB4al0i4M08169RLFyVbNV7mP4kCIPjDCwqhFcbKU9F+lU8BrHUdXM6VTujoOkn4jrNwhsDBa5tCIP08e/hzFSbZG6rbGyz1oZAdC9IEpIUbJbG7e63xx/aA0fF7MVp0vFS83ygMh+sAIC2N1o+JPAq+U1rRk1k0VLXV4Urq2irDqCIGFsZqUPPcsKR9Ct/q5njX6AyH6wNYcjNWk5Lmb/D6/zXq1N/WuudItPAs2LfeDwAJOnNuUbAKKkOoZU0IAwSCwAASDwMJYFbWTee70LlALgYWxuil4fN/lTaAeAgtAMAgsAMEgsAAEg8ACEAwCC2P1qeDxXZc3gXoILADBILAABIPAAhAMAgtAMAgsjI45lr4I/a08RnsZeMecDXgraeuoQd6k5LmfWsvAH4yw4BVzis0PpX3Uf5h/BiQRWPCIOR/w9BSbP83jAIEFP5hTbJYFTy8r6k4YCQILvliq/BQbm+2Ji3phSbSX8RqBhd6ZKV/VsfDX5oxAG4p6YWmzXu0tvQccILDQq4qp4KkvUZxM3d0NfEdgoW93Kp4KnrM1IYcRIrDQGzNaOv1UsMrbqcwYHwILvag5FTz1exQnc4u3g0AQWOjLQtLHNtebFfFNTAsef2j4eugIgYXOmang55Yvc6V0fRb1rBEhsNCHpaXXuVY6UsNIEFjolFlL1WYqeOozW3fGg8BCZ0zN6YuDl2brzkgQWOjS0tHr1l3qUFSspxeW5wgsdMJMBa8dvsWnGlt3ihaq0gvLcwQWnDPTNRdTwVNfWix1QAAILHRh2eF7sXVnwAgsOGVWpBcdWurCR7F1Z7AILDhjpoKLHt769yhOZueeqBh97Z3cDawhsODSUvU6Mdh0V1DPKqtx7R3dCywhsOBED1PBU3R1GCACC9aZadei7/tQ2qX0ru+bgD0EFlxYqr+p4Cm27gwIgQWrLuzP3qVD3zcAezj5Gda0bMrnwpOk25ODJaZFv3mzXu0c3w9aIrBg01L+TAW/b9arWf4B80FAFyvu4QiBBStMUz5fpoLvwsqM/Lbq91NLWEANC615NhV8kvTW790E6V7VYfXd3S3BFkZYsGEhu035mjoorVm95JZWXNKK+SA/lmGgAoGFViz1Z7dltlmv9iasdrqsnc0hu87ljcEOpoRozLOp4LfNerU1v97psrB6kHSTuw6eY4SFNubyYyr4tFmv5pIUxclSl4XV1816tXB5U7CPwEIjDvuz13WQdCtJpkND1UnSz0rrXLRDDhCBhaaWfd+AsTB1qxtJVfsG75XWq16kd3setywaDcMvr6+vfd8DAmN6p/swurrfrFfZ6OpRxVPBg9Jgewu0k8L8QdIkCzL4ixEWavFsKjiTJNOR4VxY3StdMLo9GVXdKh2NZavys1Y0bJL2HCMs1FIxkunSt816NTfLKv7KPf6kNHyW+RGT2ZR9q/Ia17/yozD4h8DCxcxevH/3fR/GN6X1p53SlezvRlLSu5C61eV7HH+lIO8vAgsXMf3ZH+XP5uazdaeGIZX3JGlKPctP1LBwqaX8CSspvZd9rqPojexsvr5WOnKbV/w+9IARFip5NhXsyh+sgPcPgTVCZno3Mf+4L9tH5+FUsCsHpdt29n3fCI6YEnrMfAR/eizV9MxvPffYRJdtm/lN5cdb5T/+H5NsqcO039tAHiOsDpwEz2kI3ZjHsue6XDLwLduDd44pYP+3w/vxEXsOPUJgNXQyrZqa76fB5MN6pSLPSqc8Zz8NMyG71zhHV6d+K9u6c1Lje9ZxxLrP/fpR0oukF5ZNNEdgnWFWc2fh80Hvg2gobXar/hBu5U/L476VhrvU+OeVhduL0kCTjsFWWlscq1HWsMzqaOk4Msq+DyWMqtxXhJVvR3X17aOqt+7MVH9E+lHHOuNPP+8oTqS0+J+F2S7/fYwbtgc5wsrVjCbmKxspjSWQypRu9DU/u0f50efKN//crFfLoifPbBPqyoOOo7S9+Xoc4uJX54EVxclOBIVPStcXmYWYvrQ89s1B6Sr4whqUR50s+vCwWa+mLt+AFsnjcl8RVlMRVmWypQ6FzCeKT13czBgRWOPx1o7lHM/6s/vsOrcdqMit0p83LCOwxmNRUdNYiLrVpT6bDybOMp/usRfRAQJrHB7K+jwxFWxkaUalZ5niPIezWkZgDV/pVNCgaV19V0p7cJWZi6mhVQTW8C0qNjcv5PeKfJ99Mj+/s8wUnL8MLCKwhq1qKuhLf/aQfTE/xyLLrm5kDAisYZtVPL/s4B7GYFtUzzKjW6aFlhBYw/WVqWBnPqp82w6bnS0hsIbpqawliuk0wcfudhV+Ygh7uggs/kN2b1bx/FK0jbGt7BPDsfwZcP7v6TSwmHb04mvFXre52Ntp07PSTdH7c0+a2tZY/gxcl31qaoOzzc9m2vE/Jy+OIs+b9WpS9OSI+7PbdFDa3mUnaVfVjC+Kk5mk/zi/K7/8w1UvL5f9sJYOXxvnzSqeX4qwauJBx4Da1bx2ZvtmArCUo174TkZYI/1bpW/0Z7fnSe9HUY36So30eLRMae+wpqwHFr3Ae0F/9nae9T6g9m1f0Cwm3Wm8P/PSRpFNuZgSzjXe/0h9mVX8j7EU/03yatWh6iKsJKX/7nOlXUCssTrCoqjbi++b9WpW9CRTwTdt6lAXMSPZudjulGe1AG97hLUQYdWlg0oWgI68KZ+VOlQV8zOeKl3pfiv+/z+1kMUPHqyNsFjG0Iuq/uxLSX92dzu9sl6HKmL6h90qDaqxrLFqw9ooy+YIi60e3bqkP/uQwypfh9o6DqgbHUdRLLqtby5L+WBlhMWnUJ0b61FdWR1q6/L0ZDNbmOo4iuL/63asfWJoa4Q1E/9Ru1T1qeBCwwirtzpU2WiyrZM61FTD+Nn55EppRrRuZmgrsJgOdudhwEd15etQW5cHgVKH6txcPgSWmd/zN1J3ZhXPLzu4B1uoQ43HxyhObtpO5W2MsGYWXgOX+X5BUz7f//Losg6VjaCmomThg5lazsYIrLCUTQV97c+e1aG2rhZsSm91qHxA+R7cYzRTy8Bq9Smh+UPyo80N4HKb9eqXoueiONnJj6lOVofayuGCTeltFf9U1KFC8mubkXXbEdas5fW43FPRE+Yvjr7CKqtDZQG1d/VG5t8zCykfwhn1zdRilNU2sKYtr8flykYq065uwnjQMaCoQ6GOaZuLGwfWyFq/+s51L23qULDlOoqTD01LBW1GWGWHR6JbO9ktuFOHgktZ+53a2gTWtMW1qK+wZrNZr3ZRnDyr+YjkIBNOog4F96bqIbAYYXWsYuHdXPX6Xt3LUQO7vNyCzeyLOhQaZ0ebwJq0uBbNzFTwCctmvdpGcfKHiruLPuk4xds5ur/8xuHsizoUTk2aXth4HVYUJ27OB0OZyl3vucL1xDz0qG4a2GVf1KFQqWxNYZlGgUWzvl6Vno7TBbNxOPuiDoUmGjX1azolnDS8Du19juLE6fKCU9Sh4MBEaQ+9WlwepAp3tlGcTF0Vy6lDwVdNA4tPCPt1JWkXxcmtjZEWdSj0oNFarKaB5XplNapdSforipOvku7qFtWpQ6FnjTKEKWH4vkiaR3FyJ2lZVMikDoUhILCG4UppcH2J4uSgdClD3o0IKAwAgTU8V2KKh4H6W983AACXahpYe5s3AWB09k0uIrAA9GHf5CKmhACCwQgLQB/2TS6iWwOAzjXt1tBmSlh4igsAlGicHW0Ca9/iWgDjtW96YZvActZWF8CgNc6ONoG1a3EtgPHaNb2w7VH1FN4B1NK04C61X4f10PJ6AOPSKjPaBtau5fUAxmXX5uK2gbVteT2AcWmVGa1qWJIUxcle9PwGUO15s15N2ryAjb2EjLIAXKJ1VtgIrKWF1wAwfHdtX6B1YJmjptimA6DMU5ODU0/Zai/TOjkBDJqVjGhddM9EcfIiDjoA8LPDZr2ycjSgzQZ+jLIAnGMtG2wH1sHi6wEI30E+BpY5eZhRFoC82qeSl7Hd051RFoCM1dGVZDmwGGUByLE6upIsfkqYx3YdYPRab8M5x9UxX3NHrwsgDDMXL+oksDbr1VbSvYvXBuC9+816tXPxwi4PUp2JAjwwNgc5Gl1JDgPLFNuYGgLjMrNdaM9zUnTPi+LkUdK10zcB4IOnzXp14/INXE4JM87SFoBXnP9Z7yKwAMAKAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDCc7yXsSxQnE0kTSTeSPuS+f+rvrgDrHpRuiXnMfd/bOLTUR4MNrCpRnEzNL0+/E2jwyYP5vst/d9VvynejDawqUZxkI7KJ+cpGaTLfOTQWbRyUjoak4+hob75eNuvV4/nLxo3AaiGKk9MQy063nZrvH0RrnbF50rFrwc58z6ZqkvTosl/U0BFYHTkJN+kYatJxFJdhBNe//AhIOo5+MrvcrwmhjhBYAchNTzMTvQ+4oseyx8dygtGz3odKZn/m8dPHmIYFgMAasTOjvktMLd/GrubvZzQzYgQWgGCwcBRAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADB+D8uDK0zPcwj7QAAAABJRU5ErkJggg=="}');
        expect(exerciseData["courseId"]).toEqual(1);
    });

    fit("add wall length exercise generating", async function () {
        await functions.login(browser, "admin@mbb.de", "123456");

        await browser.pause(500);

        await $("#navbar-menu-visible").$("a=Neue Aufgabe").click();
        await $("#choose-container .choose-dialog-value-row[data-value=element-wall-length-exercise]").click();

        //setGenerating
        await $("#generate-exercise-checkbox").click();

        //Set data
        await $("[name=exercise-name]").setValue("my new exercise");
        await $("[name=number-num-whole-stone-lb]").setValue("4");
        await $("[name=number-num-whole-stone-ub]").setValue("7");
        await $("[name=number-num-half-stone-lb]").setValue("6");
        await $("[name=number-num-half-stone-ub]").setValue("3");
        await $("[name=number-num-three-quarter-stone-lb]").setValue("6");
        await $("[name=number-num-three-quarter-stone-ub]").setValue("3");
        await $("[name=number-num-quarter-stone-lb]").setValue("2");
        await $("[name=number-num-quarter-stone-ub]").setValue("5");
        await $("[name=number-num-stones-sideways-lb]").setValue("1");
        await $("[name=number-num-stones-sideways-ub]").setValue("4");
        await $("[name=text-exercise-description]").setValue("this is my description for an generated exercise");
        await $("[name=text-exercise-image-description]").setValue("this is my image description for an generated exercise");

        await $("[name=image-exercise-image]").setValue(path.join(__dirname, "../img/cement.png"));

        await $("button[data-translation=save]").click();

        //wait until exercise is in DB
        expect(await $("#show-exercise-site").isDisplayed()).toBeTruthy();

        //check exercise values
        let exerciseData = await functions.queryDatabase(browser.config.mysqlConnection, "SELECT * FROM exercise ORDER BY id DESC LIMIT 1");

        console.log("exerciseData", exerciseData);


        exerciseData = exerciseData[0];
        expect(exerciseData["elementType"]).toEqual("element-wall-length-exercise");
        expect(exerciseData["name"]).toEqual("my new exercise");
        expect(exerciseData["isGenerating"]).toEqual(0);
        expect(exerciseData["generatingData"]).toEqual('{"exercise-name":"my new exercise","num-whole-stone":1,"num-three-quarter-stone":0,"num-half-stone":2,"num-quarter-stone":3,"num-stones-sideways":4,"exercise-description":"this is my description","exercise-image-description":"this is my image description","exercise-image":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAYAAAB5fY51AAAACXBIWXMAAAsSAAALEgHS3X78AAAR6UlEQVR4nO3dwXnbRhrG8Tf77F3uwNwKpFRgpoIoF+CAQ7gVmFuB6QpWrmDpAw/kZakKQlVgqYKlOhAr0B4wECGaAAhgBpgB/r/n0SOHNEhEiV/PfJz55pfX11cBQAj+1vcNAMClCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsICARXFy0/c9dOmX19fXvu8BQE1RnHyQtJT0u3no22a9mvd3R90gsIDAmFHVVtLHk6f+2KxX2x5uqTNMCYGARHGykPRDP4eVJC3NyGuw/t73DQCoFsXJROkU8FPJb7tSOvKaur+jfjDCAjwXxcmtpEeVh1XmUxQng61lUcMCPGWmdwtJnxtc/utmvXq0e0f9I7AAD5nC+lLSdcOXeJZ0s1mvXqzdlAeYEgKeMVO6nZqHlZQW5e+s3JBHGGEBnjiztsqGf27Wq6XF1+sVgQV4IIqTqdKwOrdcoY2D0qnh3vLr9oIpIdAzs7bqL9kPK+m41GEQGGEBPTFrq7ZqV6u61G+b9WrXwfs4xQgL6EFubVUXYSXzXsFjpTvQIVNYv5P0Z4dv+3UoyxsILKAjFtZW1fUs6XZIC0iZEgIdMGurfqi7sPqu9NPBwYSVxAgLcMpMAbe6bB+gDQdJ8yGtvcojsABHzNqqrdKlBV14UjoF3Hf0fp0jsAAHoji5U7NNy02NouMogQVY1PHaKimdAt4OYY3VJSi6A5ZEcTJTt2ur7iVNxhJWEiMsoDVHm5ar/GuzXg2uG0MVAgtooeRACFeeJM2GtlzhUkwJgYYqDoRw4buk6VjDSmKEBdTW09qq2dCP8LoEIyygBrNpea/uwupJ6Yr10YeVxAgLuEjLAyGa+rpZrxYdvp/3CCygQk+blmdjWq5wKaaEQAlLB0LUca90Crjr6P2CwggLOKOHtVUHSYsxrq2qg8ACTjg8EKLIqNdW1cGUEMhxfCDEOd808rVVdTDCAowoTrrcB8jaqgY4NQfQW5eF/3X0dg9KOywMos96lxhhAakPHb0Pa6taoIYFSDI1pG8O3+JZ0q+EVTsEFnC0UPqJnW3Z2ioK6y1RwwJyzKr2nez0YR/0gRB9YIQFKC26R3GSjYIWFl4y27S8tPBaMBhhYfRMB4al0i4M08169RLFyVbNV7mP4kCIPjDCwqhFcbKU9F+lU8BrHUdXM6VTujoOkn4jrNwhsDBa5tCIP08e/hzFSbZG6rbGyz1oZAdC9IEpIUbJbG7e63xx/aA0fF7MVp0vFS83ygMh+sAIC2N1o+JPAq+U1rRk1k0VLXV4Urq2irDqCIGFsZqUPPcsKR9Ct/q5njX6AyH6wNYcjNWk5Lmb/D6/zXq1N/WuudItPAs2LfeDwAJOnNuUbAKKkOoZU0IAwSCwAASDwMJYFbWTee70LlALgYWxuil4fN/lTaAeAgtAMAgsAMEgsAAEg8ACEAwCC2P1qeDxXZc3gXoILADBILAABIPAAhAMAgtAMAgsjI45lr4I/a08RnsZeMecDXgraeuoQd6k5LmfWsvAH4yw4BVzis0PpX3Uf5h/BiQRWPCIOR/w9BSbP83jAIEFP5hTbJYFTy8r6k4YCQILvliq/BQbm+2Ji3phSbSX8RqBhd6ZKV/VsfDX5oxAG4p6YWmzXu0tvQccILDQq4qp4KkvUZxM3d0NfEdgoW93Kp4KnrM1IYcRIrDQGzNaOv1UsMrbqcwYHwILvag5FTz1exQnc4u3g0AQWOjLQtLHNtebFfFNTAsef2j4eugIgYXOmang55Yvc6V0fRb1rBEhsNCHpaXXuVY6UsNIEFjolFlL1WYqeOozW3fGg8BCZ0zN6YuDl2brzkgQWOjS0tHr1l3qUFSspxeW5wgsdMJMBa8dvsWnGlt3ihaq0gvLcwQWnDPTNRdTwVNfWix1QAAILHRh2eF7sXVnwAgsOGVWpBcdWurCR7F1Z7AILDhjpoKLHt769yhOZueeqBh97Z3cDawhsODSUvU6Mdh0V1DPKqtx7R3dCywhsOBED1PBU3R1GCACC9aZadei7/tQ2qX0ru+bgD0EFlxYqr+p4Cm27gwIgQWrLuzP3qVD3zcAezj5Gda0bMrnwpOk25ODJaZFv3mzXu0c3w9aIrBg01L+TAW/b9arWf4B80FAFyvu4QiBBStMUz5fpoLvwsqM/Lbq91NLWEANC615NhV8kvTW790E6V7VYfXd3S3BFkZYsGEhu035mjoorVm95JZWXNKK+SA/lmGgAoGFViz1Z7dltlmv9iasdrqsnc0hu87ljcEOpoRozLOp4LfNerU1v97psrB6kHSTuw6eY4SFNubyYyr4tFmv5pIUxclSl4XV1816tXB5U7CPwEIjDvuz13WQdCtJpkND1UnSz0rrXLRDDhCBhaaWfd+AsTB1qxtJVfsG75XWq16kd3setywaDcMvr6+vfd8DAmN6p/swurrfrFfZ6OpRxVPBg9Jgewu0k8L8QdIkCzL4ixEWavFsKjiTJNOR4VxY3StdMLo9GVXdKh2NZavys1Y0bJL2HCMs1FIxkunSt816NTfLKv7KPf6kNHyW+RGT2ZR9q/Ia17/yozD4h8DCxcxevH/3fR/GN6X1p53SlezvRlLSu5C61eV7HH+lIO8vAgsXMf3ZH+XP5uazdaeGIZX3JGlKPctP1LBwqaX8CSspvZd9rqPojexsvr5WOnKbV/w+9IARFip5NhXsyh+sgPcPgTVCZno3Mf+4L9tH5+FUsCsHpdt29n3fCI6YEnrMfAR/eizV9MxvPffYRJdtm/lN5cdb5T/+H5NsqcO039tAHiOsDpwEz2kI3ZjHsue6XDLwLduDd44pYP+3w/vxEXsOPUJgNXQyrZqa76fB5MN6pSLPSqc8Zz8NMyG71zhHV6d+K9u6c1Lje9ZxxLrP/fpR0oukF5ZNNEdgnWFWc2fh80Hvg2gobXar/hBu5U/L476VhrvU+OeVhduL0kCTjsFWWlscq1HWsMzqaOk4Msq+DyWMqtxXhJVvR3X17aOqt+7MVH9E+lHHOuNPP+8oTqS0+J+F2S7/fYwbtgc5wsrVjCbmKxspjSWQypRu9DU/u0f50efKN//crFfLoifPbBPqyoOOo7S9+Xoc4uJX54EVxclOBIVPStcXmYWYvrQ89s1B6Sr4whqUR50s+vCwWa+mLt+AFsnjcl8RVlMRVmWypQ6FzCeKT13czBgRWOPx1o7lHM/6s/vsOrcdqMit0p83LCOwxmNRUdNYiLrVpT6bDybOMp/usRfRAQJrHB7K+jwxFWxkaUalZ5niPIezWkZgDV/pVNCgaV19V0p7cJWZi6mhVQTW8C0qNjcv5PeKfJ99Mj+/s8wUnL8MLCKwhq1qKuhLf/aQfTE/xyLLrm5kDAisYZtVPL/s4B7GYFtUzzKjW6aFlhBYw/WVqWBnPqp82w6bnS0hsIbpqawliuk0wcfudhV+Ygh7uggs/kN2b1bx/FK0jbGt7BPDsfwZcP7v6TSwmHb04mvFXre52Ntp07PSTdH7c0+a2tZY/gxcl31qaoOzzc9m2vE/Jy+OIs+b9WpS9OSI+7PbdFDa3mUnaVfVjC+Kk5mk/zi/K7/8w1UvL5f9sJYOXxvnzSqeX4qwauJBx4Da1bx2ZvtmArCUo174TkZYI/1bpW/0Z7fnSe9HUY36So30eLRMae+wpqwHFr3Ae0F/9nae9T6g9m1f0Cwm3Wm8P/PSRpFNuZgSzjXe/0h9mVX8j7EU/03yatWh6iKsJKX/7nOlXUCssTrCoqjbi++b9WpW9CRTwTdt6lAXMSPZudjulGe1AG97hLUQYdWlg0oWgI68KZ+VOlQV8zOeKl3pfiv+/z+1kMUPHqyNsFjG0Iuq/uxLSX92dzu9sl6HKmL6h90qDaqxrLFqw9ooy+YIi60e3bqkP/uQwypfh9o6DqgbHUdRLLqtby5L+WBlhMWnUJ0b61FdWR1q6/L0ZDNbmOo4iuL/63asfWJoa4Q1E/9Ru1T1qeBCwwirtzpU2WiyrZM61FTD+Nn55EppRrRuZmgrsJgOdudhwEd15etQW5cHgVKH6txcPgSWmd/zN1J3ZhXPLzu4B1uoQ43HxyhObtpO5W2MsGYWXgOX+X5BUz7f//Losg6VjaCmomThg5lazsYIrLCUTQV97c+e1aG2rhZsSm91qHxA+R7cYzRTy8Bq9Smh+UPyo80N4HKb9eqXoueiONnJj6lOVofayuGCTeltFf9U1KFC8mubkXXbEdas5fW43FPRE+Yvjr7CKqtDZQG1d/VG5t8zCykfwhn1zdRilNU2sKYtr8flykYq065uwnjQMaCoQ6GOaZuLGwfWyFq/+s51L23qULDlOoqTD01LBW1GWGWHR6JbO9ktuFOHgktZ+53a2gTWtMW1qK+wZrNZr3ZRnDyr+YjkIBNOog4F96bqIbAYYXWsYuHdXPX6Xt3LUQO7vNyCzeyLOhQaZ0ebwJq0uBbNzFTwCctmvdpGcfKHiruLPuk4xds5ur/8xuHsizoUTk2aXth4HVYUJ27OB0OZyl3vucL1xDz0qG4a2GVf1KFQqWxNYZlGgUWzvl6Vno7TBbNxOPuiDoUmGjX1azolnDS8Du19juLE6fKCU9Sh4MBEaQ+9WlwepAp3tlGcTF0Vy6lDwVdNA4tPCPt1JWkXxcmtjZEWdSj0oNFarKaB5XplNapdSforipOvku7qFtWpQ6FnjTKEKWH4vkiaR3FyJ2lZVMikDoUhILCG4UppcH2J4uSgdClD3o0IKAwAgTU8V2KKh4H6W983AACXahpYe5s3AWB09k0uIrAA9GHf5CKmhACCwQgLQB/2TS6iWwOAzjXt1tBmSlh4igsAlGicHW0Ca9/iWgDjtW96YZvActZWF8CgNc6ONoG1a3EtgPHaNb2w7VH1FN4B1NK04C61X4f10PJ6AOPSKjPaBtau5fUAxmXX5uK2gbVteT2AcWmVGa1qWJIUxcle9PwGUO15s15N2ryAjb2EjLIAXKJ1VtgIrKWF1wAwfHdtX6B1YJmjptimA6DMU5ODU0/Zai/TOjkBDJqVjGhddM9EcfIiDjoA8LPDZr2ycjSgzQZ+jLIAnGMtG2wH1sHi6wEI30E+BpY5eZhRFoC82qeSl7Hd051RFoCM1dGVZDmwGGUByLE6upIsfkqYx3YdYPRab8M5x9UxX3NHrwsgDDMXL+oksDbr1VbSvYvXBuC9+816tXPxwi4PUp2JAjwwNgc5Gl1JDgPLFNuYGgLjMrNdaM9zUnTPi+LkUdK10zcB4IOnzXp14/INXE4JM87SFoBXnP9Z7yKwAMAKAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDCc7yXsSxQnE0kTSTeSPuS+f+rvrgDrHpRuiXnMfd/bOLTUR4MNrCpRnEzNL0+/E2jwyYP5vst/d9VvynejDawqUZxkI7KJ+cpGaTLfOTQWbRyUjoak4+hob75eNuvV4/nLxo3AaiGKk9MQy063nZrvH0RrnbF50rFrwc58z6ZqkvTosl/U0BFYHTkJN+kYatJxFJdhBNe//AhIOo5+MrvcrwmhjhBYAchNTzMTvQ+4oseyx8dygtGz3odKZn/m8dPHmIYFgMAasTOjvktMLd/GrubvZzQzYgQWgGCwcBRAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADBILAABIPAAhAMAgtAMAgsAMEgsAAEg8ACEAwCC0AwCCwAwSCwAASDwAIQDAILQDAILADB+D8uDK0zPcwj7QAAAABJRU5ErkJggg=="}');
        expect(exerciseData["courseId"]).toEqual(1);
    });
});