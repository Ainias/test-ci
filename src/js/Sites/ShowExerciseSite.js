import view from "../../html/sites/showExerciseSite.html"
import {ExerciseFragment} from "../Fragments/ExerciseFragment";
import {App, ConfirmDialog, MenuAction, MenuSite, Toast} from "cordova-sites/dist/client";
import {Helper} from "js-helper/dist/shared";
import {Calculator} from "../Dialog/Calculator";
import {Exercise} from "../../../model/Exercise.ts";
import {UserSite} from "cordova-sites-user-management/dist/client";
import {ExerciseHelper} from "../Helpers/ExerciseHelper";
import {WrongAnswer} from "../../../model/WrongAnswer";
import {UserManager, UserMenuAction} from "cordova-sites-user-management/dist/client";
import {EditExerciseSite} from "./EditExerciseSite";
import {User} from "cordova-sites-user-management/dist/shared/v1/model/User";

export class ShowExerciseSite extends MenuSite {

    constructor(siteManager) {
        super(siteManager, view);

        this._exerciseFragment = new ExerciseFragment(this);
        this.addFragment("#show-exercise-site", this._exerciseFragment);
        this.addDelegate(new UserSite(this, "loggedIn", true));

        this._exercise = null;
    }

    onCreateMenu(navbar) {
        super.onCreateMenu(navbar);
        navbar.addAction(new MenuAction("reset", async () => {
            try {
                this.showLoadingSymbol();

                await this._saveExercise();

                let newP = await ExerciseHelper.createNextProgressFor(this._exercise);
                await newP.save();

                let exerciseId = this._exercise.getId();
                this._exercise = null;

                await this.finishAndStartSite(ShowExerciseSite, {"exerciseId": exerciseId})
            } catch (e) {
                console.error(e);
            }
        }));
        navbar.addAction(new UserMenuAction("edit", "admin", async () => {
            this.finishAndStartSite(EditExerciseSite, {"exercise": this._exercise.id});
        }));
        navbar.addAction(new UserMenuAction("remove", "admin", async () => {
            if (await new ConfirmDialog("do you want to delete exercise", "delete exercise").show()) {
                this._exercise.delete();
                new Toast("exercise successfully deleted").show();
                this.finish();
            }
        }));
    }

    async _saveExercise(progress) {
        if (Helper.isNull(progress) && Helper.isNotNull(this._exercise)) {
            progress = await ExerciseHelper.getProgressFor(this._exercise);
        }

        if (Helper.isNotNull(progress)) {

            if (!progress.isDone) {
                if (Helper.isNotNull(this._currentStartTime)) {
                    let currentTimeNeeded = new Date().getTime() - this._currentStartTime.getTime();
                    progress.timeNeeded += currentTimeNeeded;
                    this._currentStartTime = new Date();
                }
                await progress.save();
            }

            if (progress._wrongAnswers) {
                await WrongAnswer.saveMany(progress._wrongAnswers);
            }

            return progress;
        }

        return null;
    }

    async isDone(state) {
        let progress = await this._saveExercise();
        if (Helper.isNotNull(progress)) {
            if (Helper.isNotNull(state)) {
                progress.state = state;
            }
            progress.isDone = true;

            let user = new User();
            user.id = UserManager.getInstance().getUserData().id;
            progress.user = user;

            let saveProgressPromise = progress.save(false);
            let wrongAnswers = await WrongAnswer.find({exerciseProgress: {clientId: progress.clientId}});

            await saveProgressPromise;
            wrongAnswers.forEach(wrongAnswer => {
                wrongAnswer.exerciseProgress = progress;
                wrongAnswer.user = user;
            });
            await WrongAnswer.saveMany(wrongAnswers, false);
        }
    }

    async onConstruct(constructParameters) {
        let res = super.onConstruct(constructParameters);

        if (Helper.isNotNull(constructParameters) && constructParameters["exerciseId"]) {
            this._exercise = await Exercise.findById(constructParameters["exerciseId"]);
        }

        if (Helper.isNull(this._exercise)) {
            console.warn("no exercise found! ", constructParameters);
            res.then(() => {
                this.finish();
            });
            return res;
        }
        await this._exerciseFragment.setExercise(this._exercise);

        return res;
    }

    async onStart(pauseArguments) {
        await super.onStart(pauseArguments);
        if (this._exercise) {
            this._currentStartTime = new Date();
        }
    }

    async onPause() {
        let res = super.onPause();
        await this._saveExercise();
        return res;
    }

    onBackPressed() {
        new ConfirmDialog("abort exercise", "abort-exercise-title").show().then(res => {
            if (res) {
                this.finish();
            }
        });
        return false;
    }
}

App.addInitialization((app) => {
    app.addDeepLink("exercise", ShowExerciseSite);
});