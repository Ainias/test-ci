import {BuilderFragment} from "../BuilderFragment";
import view from "../../../../html/Fragments/ExerciseFragments/Exercises/pictureFragment.html"
import {Lexicon} from "../../../Lexicon";

export class PictureFragment extends BuilderFragment {

    constructor(site) {
        super(site, view);
    }

    async setData(data) {
        this._source = data["source"];
        this._description = data["description"];

        let img = await this.findBy(".image-to-show", null, true);
        img.src = this._source;
        img.title = this._description;
        img.alt = this._description;

        (await this.findBy(".image-description", null, true)).appendChild(await Lexicon.getInstance().prepareText(this._description));
    }
}