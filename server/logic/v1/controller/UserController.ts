import {UserManager} from "cordova-sites-user-management/dist/server/v1/UserManager";
import {User} from "cordova-sites-user-management/dist/shared/v1/model/User";
import {Helper} from "js-helper";
import {Like} from "typeorm";

export class UserController {
    static async listUsers(req, res){
        if (!await UserManager.hasAccess(req.user, "admin")) {
            throw new Error("no rights for list users!");
        }

        let userSearchName = Helper.nonNull(req.query.username, "");

        let users = await User.find({username: Like("%"+userSearchName+"%")});
        res.json({
            success: true,
            data: users
        });
    }
}