const mysql = require("mysql");
const childProcess = require("child_process");
const fs = require("fs");

let db = "mbb_test";
let pw = "123456";
let mysqlConn = mysql.createConnection({
    host: "localhost",
    "user": "root",
    "password": pw,
    "database": db,
    "multipleStatements": true
});

let child = null;

async function setup() {
    await generateDb();
    await startTestServer();
}

async function tearDown() {
    if (child){
        //TODO kill for real!
        console.log("killing child...");
        child.kill("SIGKILL");
    }
}

async function startTestServer() {
    return new Promise((resolve, reject) => {
        child = childProcess.exec("npm run server", {
            env: Object.assign({}, process.env,{
                MYSQL_PASSWORD: pw,
                MYSQL_DATABASE: db,
                JWT_SECRET: "123456",
                PEPPER: "123456"
            }),
            stdio: "pipe"
        }, reject);
        child.stdout.on("data", data => {
            console.log("[SERVER]", data);
            if (data.indexOf("Server started on Port: ") !== -1){
                resolve();
            }
        });
    })
}

async function generateDb() {

    let sqlSchema = fs.readFileSync(__dirname + '/dumpSchema20191219.sql', 'utf-8');
    let sqlData = fs.readFileSync(__dirname + '/setup.sql', 'utf-8');

    // console.log(sqlStrings);
    // sqlStrings = sqlStrings.split(";");

    let mysqlPromise = Promise.resolve();
    // sqlStrings.forEach((sql) => {
    //     if (sql.trim() !== "") {
    mysqlPromise = mysqlPromise.then(() => new Promise(r => {
        mysqlConn.query(sqlSchema + ";", function (err, result) {
            if (!err) {
                r(new Promise(r => {
                    mysqlConn.query(sqlData + ";", function (err, result) {
                        if (!err){
                            r(result);
                        }
                        else{
                            console.error(err);
                            throw err;
                        }
                    });
                }));
            } else {
                console.error(err);
                throw err;
            }
        });
    }));
    // }
    // });
    await mysqlPromise;
    console.log("mysqlPromise resolved!");
    return true;
}

class InitService {
    async onPrepare(config, capabilities) {
        console.log("onPrepare", config, capabilities, new Date());
        await setup();
        console.log("onPrepare2", new Date());
    }

    async onComplete(exitCode, config, capabilities) {
        console.log("onComplete", exitCode, config, capabilities, new Date());
        await tearDown();
        console.log("onComplete2", new Date());
    }
}


module.exports = {setup: setup, tearDown: tearDown, service: InitService, mysqlConnection: mysqlConn};
