import {MenuSite} from "cordova-sites/dist/client/js/Context/MenuSite";

import view from "../../html/sites/listWikiEntries.html"
import {WikiEntry} from "../../../model/WikiEntry";
import {WikiSite} from "./WikiSite";
import {UserMenuAction} from "cordova-sites-user-management/dist/client/js/MenuAction/UserMenuAction";
import {EditWikiEntrySite} from "./EditWikiEntrySite";
import {ViewHelper} from "js-helper/dist/client/ViewHelper";

export class ListWikiEntries extends MenuSite {

    constructor(siteManager) {
        super(siteManager, view);
    }

    async onConstruct(constructParameters) {
        let res = super.onConstruct(constructParameters);

        this._wikiEntries = await WikiEntry.find();

        return res;
    }

    async onViewLoaded() {
        let res = super.onViewLoaded();

        this._wikiEntryContainer = this.findBy("#wiki-entry-container");
        this._wikiEntryTemplate = this.findBy("#wiki-entry-template");

        this._wikiEntryTemplate.remove();
        this._wikiEntryTemplate.removeAttribute("id");

        return res;
    }

    async onStart(pauseArguments) {
        super.onStart(pauseArguments);
        this.addWikiEntries();
    }

    onCreateMenu(navbar) {
        super.onCreateMenu(navbar);
        navbar.addAction(new UserMenuAction("new entry", "admin", () => {
            this.startSite(EditWikiEntrySite);
        }))
    }

    addWikiEntries(){
        ViewHelper.removeAllChildren(this._wikiEntryContainer);
        this._wikiEntries.forEach(entry => {
            let element = this._wikiEntryTemplate.cloneNode(true);
            element.querySelector(".name").innerText = entry.name;

            element.addEventListener("click", () => {
                this.finishAndStartSite(WikiSite, {id: entry.id});
            });

            let prefix = entry.description.length > 100?"...":"";

            let previewDiv = document.createElement("div");
            previewDiv.innerHTML = entry.description;
            previewDiv.innerHTML = previewDiv.innerText.substr(0,100)+prefix;

            element.appendChild(previewDiv);

            console.log(previewDiv.innerHTML);

            this._wikiEntryContainer.appendChild(element);
        });
    }
}