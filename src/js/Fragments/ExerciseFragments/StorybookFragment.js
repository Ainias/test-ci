import {AbstractFragment} from "cordova-sites/dist/client/js/Context/AbstractFragment";

import view from "../../../html/Fragments/ExerciseFragments/Exercises/storybookFragment.html"
import {ExerciseHelper} from "../../Helpers/ExerciseHelper";
import {Toast} from "cordova-sites/dist/client/js/Toast/Toast";

export class StorybookFragment extends AbstractFragment{

    constructor(site) {
        super(site, view);
    }

    onViewLoaded() {
        let res = super.onViewLoaded();

        this._storybookPlayer = new GB.GBPlayer(this.findBy("#storybook-container"), r =>  {
            console.log("done", r);
            new Toast("finish!").show();
        }, "de");

        if (this._exercise){
            debugger;
            this._storybookPlayer.LoadGamebook(this._exercise.generatingData);
        }

        return res;
    }

    async setElement(exercise) {
        this._exercise = exercise;
        if (this._storybookPlayer){
            this._storybookPlayer.LoadGamebook(exercise.generatingData);
        }
    }
}