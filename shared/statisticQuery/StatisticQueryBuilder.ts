import {Brackets, QueryBuilder, SelectQueryBuilder} from "typeorm";
import {WrongAnswer} from "../../model/WrongAnswer";
import {Exercise} from "../../model/Exercise";
import {Helper} from "js-helper/dist/shared/Helper";

export class StatisticQueryBuilder {
    static async getSummedData(queryBuilder: QueryBuilder<any>, start: string, end?: string, userId?: number, exerciseId?: number) {

        let res = await this._getOverviewData(queryBuilder, start, end, userId, exerciseId);
        let data = await this.getData(queryBuilder.createQueryBuilder(), start, end, userId, exerciseId);

        if (res) {
            res["data"] = data;
            return res;
        } else {
            return null;
        }
    }

    static async _getOverviewData(queryBuilder, start, end, userId, exerciseId) {
        queryBuilder = queryBuilder.select("DISTINCT 'placeholder'", "placeholder").addSelect(qb => {
            qb = qb.select("COUNT(wrong_answer.id)", "num")
                .from("wrong_answer", "wrong_answer");

            if (Helper.isNotNull(exerciseId)) {
                qb = qb.innerJoin("wrong_answer.exerciseProgress", "exercise_progress");
            }

            return this._addWheres(qb, start, end, userId, exerciseId, "wrong_answer", true);
        }, "number-wrong-answers").addSelect(qb => {
            qb = qb.select("COUNT(id)", "num")
                .from("exercise_progress", "exercise_progress")
                .where(new Brackets(bqb => {
                        bqb.where("id NOT IN " + qb.subQuery()
                            .select("MAX(id)")
                            .from("exercise_progress", "exp2")
                            .groupBy("elementId, userId").getQuery())
                            .orWhere("isDone = 1")
                    }
                ));
            return this._addWheres(qb, start, end, userId, exerciseId);
        }, "number-exercises-executed").addSelect(qb => {
            qb = qb.select("COUNT(id)", "num")
                .from("exercise_progress", "exercise_progress");
            return this._addWheres(qb, start, end, userId, exerciseId, null, true);
        }, "number-exercises").addSelect(qb => {
            qb = qb.select("COUNT(id)", "num")
                .from("exercise_progress", "exercise_progress")
                .where("isDone = 1");
            return this._addWheres(qb, start, end, userId, exerciseId);
        }, "number-exercises-done").addSelect(qb => {
            qb = qb.select("SUM(timeNeeded)", "num")
                .from("exercise_progress", "exercise_progress")
                .where("isDone = 1");
            return this._addWheres(qb, start, end, userId, exerciseId);
        }, "time-exercises-done").addSelect(qb => {
            qb = qb.select("SUM(timeNeeded)", "num")
                .from("exercise_progress", "exercise_progress")
                .where(new Brackets(bqb => {
                        bqb.where("id NOT IN " + qb.subQuery()
                            .select("MAX(id)")
                            .from("exercise_progress", "exp2")
                            .groupBy("elementId, userId").getQuery())
                            .andWhere("isDone = 0")
                    }
                ));
            return this._addWheres(qb, start, end, userId, exerciseId);
        }, "time-exercises-aborted").from("exercise", "exercise");

        // console.log("SQL", queryBuilder.getSql());

        let queryRes = await queryBuilder.execute();

        if (queryRes.length > 0) {
            return queryRes[0];
        } else {
            return null;
        }
    }

    static async getData(queryBuilder, start, end, userId, exerciseId?) {
        queryBuilder = queryBuilder.select("DATE(WrongAnswer.occurredAt)", "wrongAnswerOccurredAt")
            .addSelect("WrongAnswer.field", "field")
            .addSelect("WrongAnswer.given", "given")
            .addSelect("WrongAnswer.expected", "expected")
            .addSelect("Exercise.id", "exerciseId")
            .addSelect("Exercise.name", "exerciseName")
            .addSelect("Exercise.isGenerating", "exerciseIsGenerating")
            .from("WrongAnswer", "WrongAnswer")
            .innerJoin("WrongAnswer.exerciseProgress", "exercise_progress")
            .innerJoin("exercise_progress.element", "Exercise")
            .where("1 = 1");
        queryBuilder = this._addWheres(queryBuilder, start, end, userId, exerciseId, "WrongAnswer");
        // console.log(queryBuilder.getSql());
        return queryBuilder.getRawMany();
    }

    static _addWheres(qb, start, end, userId, exerciseId?, table?, startWithNormalWhere?) {
        if (!table) {
            table = "";
        } else {
            table += ".";
        }

        if (Helper.nonNull(startWithNormalWhere, false)) {
            qb = qb.andWhere(table + "updatedAt >= :start");
        } else {
            qb = qb.where(table + "updatedAt >= :start");
        }

        qb.setParameter("start", start);
        if (userId) {
            qb = qb.andWhere(table + "userId = :userId");
            qb.setParameter("userId", userId);
        }
        if (end) {
            qb = qb.andWhere(table + "updatedAt <= :end");
            qb.setParameter("end", end);
        }
        if (exerciseId){
            qb = qb.andWhere("exercise_progress.elementId = :exerciseId");
            qb.setParameter("exerciseId", exerciseId)
        }
        return qb;
    }

    static async getExerciseStatistic(queryBuilder: SelectQueryBuilder<any>, start, end, userId, exerciseId) {
        let res = this._getOverviewData(queryBuilder, start, end, userId, exerciseId);

        if (res) {
            return res;
        } else {
            return null;
        }
    }
}