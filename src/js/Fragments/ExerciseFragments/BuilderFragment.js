import {AbstractFragment} from "cordova-sites/dist/client";

export class BuilderFragment extends AbstractFragment{
    async setData(data){
        throw new Error("must be overloaded, dont call parent-method from child!");
    }
}