import {App, Helper, Translator} from "cordova-sites/dist/client";
import ttsImg from "../../img/speaker.svg";
import {ViewHelper} from "js-helper/dist/client/ViewHelper";

export class TextSpeaker {

    constructor() {
        this.isBrowser = (device.platform.toLowerCase() === "browser");
        this._speakingElement = null;
    }

    static getLocale() {
        let locale = Translator.getInstance().getCurrentLanguage();
        locale += (locale === "de") ? "-DE" : "-US";
        return locale;
    }

    async speak(message, options) {
        options = Helper.nonNull(options, {});
        options["rate"] = Helper.nonNull(options["rate"], 0.85);
        options["locale"] = Helper.nonNull(options["locale"], "de-DE");
        options["volume"] = Helper.nonNull(options["volume"], 1);
        options["pitch"] = Helper.nonNull(options["pitch"], 1);

        if (this.isBrowser) {
            if ("speechSynthesis" in window) {
                let msg = new SpeechSynthesisUtterance(message);
                msg.rate = options["rate"]; // 0.1 to 10
                msg.lang = options["locale"];
                msg.volume = options["volume"];
                msg.pitch = options["pitch"];
                msg.onstart = options["onstart"];

                return new Promise((resolve, reject) => {
                    msg.onend = resolve;
                    msg.onerror = reject;
                    window.speechSynthesis.cancel();
                    window.speechSynthesis.speak(msg);
                });
            } else {
                return Promise.reject("no speechSynthesis api");
            }
        } else {
            options["text"] = message;
            return new Promise((resolve, reject) => {
                TTS.speak(options, resolve, reject);
                if (typeof options["onstart"] === "function"){
                    options["onstart"]();
                }
            });
        }
    }

    static getInstance() {
        if (Helper.isNull(TextSpeaker._instance)) {
            TextSpeaker._instance = new TextSpeaker();
        }
        return TextSpeaker._instance;
    }

    addSpeakers(element) {
        element.querySelectorAll(".textToSpeech").forEach(element => {
            if (!element.querySelector(".ttsButton")) {
                let newChild = document.createElement("span");
                newChild.classList.add("tts-text");
                ViewHelper.moveChildren(element, newChild);
                element.appendChild(newChild);

                let ttsButton = document.createElement("img");
                ttsButton.src = ttsImg;
                ttsButton.classList.add("ttsButton");
                ttsButton.addEventListener("click", async (e) => {
                    try {
                        let locale = "de-DE";
                        if (element.classList.contains("translation")) {
                            locale = TextSpeaker.getLocale();
                        }
                        e.preventDefault();
                        e.stopPropagation();

                        if (this._speakingElement){
                            this._speakingElement.classList.remove("speaking");
                        }

                        this._speakingElement = element;
                        element.classList.add("speaking");
                        await this.speak(newChild.innerText, {locale: locale}).catch(e => console.error(e));
                        element.classList.remove("speaking");

                    } catch (e) {
                        console.error(e);
                    }
                });
                // ttsButton.innerText = "sound";
                element.appendChild(ttsButton);
                element.classList.add("hasTextToSpeechButton");
            }
        });
    }
}

App.addInitialization((app) => {
    app.ready(() => {
        Translator.addTranslationCallback((baseElement) => {
            if (typeof baseElement !== "undefined") {
                TextSpeaker.getInstance().addSpeakers(baseElement);
            }
        })
    })
});

TextSpeaker._instance = null;