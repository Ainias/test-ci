const find = require("../lib/PromiseSelector");
const $ = find.one;

async function login(browser, email, password) {
    // await browser.url(baseUrl+"?s=login");
    if (await $("span=Login").isDisplayed()) {
        await $("span=Login").click();
        await $("input[name=email]").setValue(email);
        await $("input[name=password]").setValue(password);
        await $("button=Login").click();
        await browser.pause(3000);
    }
}

async function queryDatabase(mysql, query) {
    return new Promise(resolve => {
        mysql.query(query, function (err, result) {
            if (!err) {
                resolve(result);
            } else {
                console.error(err);
                resolve(err);
            }
        });
    });
}

module.exports = {
    login: login,
    queryDatabase: queryDatabase,
};