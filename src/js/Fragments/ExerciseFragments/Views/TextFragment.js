import {BuilderFragment} from "../BuilderFragment";
import {Lexicon} from "../../../Lexicon";

import view from "../../../../html/Fragments/ExerciseFragments/Exercises/textFragment.html";

export class TextFragment extends BuilderFragment{
    constructor(site) {
        super(site, view);
    }

    async setData(data) {
        this._textElement = await Lexicon.getInstance().prepareText(data["text"]);
        (await this.findBy(".text", null, true)).appendChild(this._textElement);
    }
}