# Template für Mein Beruf Bau #

* Getestet mit Versionen
    * node: v10.15.0,
    * npm: 6.4.1
    * cordova: 8.1.2

* Wie installieren? (Angenommen cordova, node, npm und git sind bereits installiert)
    * auschecken
    * führe aus: `cordova prepare`
    * führe aus: `npm install`
   
* Wie testet/führt man es aus?
    * führe aus: `cordova run <platform>` wo platform für `browser`, `android`
      oder `ios` steht. 
    * durch `cordova prepare browser` wird die Webseite neu gebuildet und der
      Server, welcher durch `cordova run browser` gestartet wurde, liefert
      die neu gebuildeten Daten aus
      
* Inklusive Usermanagement mit zwei Default-Usern (zum Testen)
    * User 1: 
        * email: user@mbb.de
        * passwort: 123456789
    * User 2:
        * email: admin@mbb.de
        * passwort: 123456       

* Benutzt Webpack mit Babel
    * Bundled JS aus verschiedene Dateien (Webpack)
    * Konvertiert JS runter für ältere Browser & fügt Polyfills ein (Babel)
        * dafür mode in webpack.config.js auf "production" stellen
* Inklusive Datenbank
    * TypeORM 
        * Benutzt SQLite auf iOS & Android
        * Benutzt SQLite.js mit localforage (IndexedDB) im Browser (etwas langsam) 
        * Daten werden von WebView nicht einfach so gelöscht
    * NativeStorage 
        * Promise-based Ersatzt für LocalStorage 
        * Daten werden von iOS nicht einfach so gelöscht


## Server ##
Die Server-Sourcen liegen in server/

Zum Konfigurieren kann eine Datei im Root (nicht in server/) mit dem Namen .env (ja, nur .env) angelegt werden (bitte nicht einchecken).
Dazu einfach `VARNAME=Value` in eine Zeile in der .env schreiben. Leerzeilen werden ignoriert und Zeilen mit # am Anfang als Kommentare angesehen.

Die Variablen sind dann in Node unter `process.env.VARNAME` zu finden. Sollte es eine Systemvaraible mit dem Namen bereits geben,
so wird diese genommen und der Wert in der Datei ignoriert.

Momentan erwartet der Server folgende Variablen:
- `PORT` (Default: 3000),
- `MYSQL_HOST` (Default: "localhost""), 
- `MYSQL_PORT` (Default: "3306"),
- `MYSQL_USER` (Default: "root"),
- `MYSQL_PASSWORD` (Default: ""),
- `MYSQL_DATABASE` (Default: "mbb"),
- `JWT_SECRET` (Default: "mySecretioöqwe78034hjiodfu80ä^", wird zum Verschlüsseln des Javascript-Web-Token genutzt. Sollte für Produktions-Umgebung auf jedenfall geändert werden. Wird der Wert geändert, werden alle Token ungültig (Alle User werden ausgeloggt))
- `PEPPER` (Default: "mySecretPepper", wird zum Hashen der Passwörter verwendet. Sollte für Produktionsumgebung auf jedenfall geändert werden. Wird der Wert geändert, werden alle Passwörter ungültig!) 

Damit der Server richtig funktioniert, muss der MySQL-Server im Hintergrund laufen und die Datenbank muss angelegt sein. Die Tabellen werden von alleine angelegt.

Der Server wird mit `npm run server` gestartet

## Client ##

Die Client-Sourcen für das Projekt liegen in src. Alles in www wird generiert und sollte daher nicht angepasst werden.

Das Framework teilt die Ansichten in "Seiten" auf. Der Lifecycle einer Seite sieht wie folgt aus:
* onConstruct
* onViewLoaded
* onStart
* onPause
* onDestroy

Die Funktionen werden im Normalfall in dieser Reihenfolge aufgerufen. onConstruct und onViewLoaded können 
ein Promise zurückgeben, welche erst resolved werden müssen, bevor onStart aufgerufen wird.
 
onStart und onPause können mehrmals in einem Lifecycle aufgerufen werden. Angenommen Seite A wird gestartet,
und startet nach 5 Sekunden Seite B. Seite B endet nach 10 Sekunden. Nach weiteren 7 Sekunden endet Seite A.
Der Lifecycle sieht wie folgt aus:

* SiteA.onConstruct
* SiteA.onViewLoaded
* SiteA.onStart
* _5 Sekunden warten_ SiteA startet SiteB
* _zeige Loading-Symbol_
* SiteB.onConstruct  
* SiteB.onViewLoaded
* SiteA.onPause
* SiteB.onStart
* _10 Sekunden warten_
* SiteB.onPause
* _zeige Loading-Symbol_
* SiteB.onDestroy (Warten, bis Promise von onDestroy aufgelöst)
* SiteA.onStart
* _7 Sekunden warten_
* SiteA.onPause
* SiteA.onDestroy

onStart erhält dabei immer den Rückgabewert von onPause zuvor zurückgeben wurden.

In welcher Phase eine Seite ist, kann durch die Variable site._state herausgefunden werden.

Außerdem kann eine Seite Fragmente enthalten, die den gleichen Lifecycle wie die Seite haben. Sie hängen
an der Seite, können jedoch selber auch noch mal Sub-Fragmente enthalten. 

Es gibt bereits eine MenuSite, welche ein NavbarFragment enthält. Diese MenuSite hat eine weitere Funktion 
onCreateMenu, welche innerhalb von onConstruct aufgerufen wird und für die Erstellung des Navbarmenus 
zuständig ist.    

Zusätzlich gibt es einen Translator, welcher es ermöglicht leicht die Texte zu übersetzten. 
Soll er direkt in HTML genutzt werden, so muss das entsprechende Element die Klasse "translation" besitzen. Der 
Text innerhalb des Elements oder innerhalb des Attributes data-translation wird dann als Key gewählt und 
entsprechend übersetzt. Groß/Kleinschreibung bei dem Key ist egal. Zusätzlich können mit 
data-translation-args mehrere Attribute als JSON-Array codiert an die Übersetzung gegeben werden. Mit {0}, 
{1}, ... greift man auf die verschiedene Elemente in der Übersetzung zurück.

Außerdem können attribute ebenfalls hiermit übersetzt werden. Hierfür muss das Attribut "data-translation-\<attributname\>"
mit dem entsprechenden Key versehen werden. Mithilfe von "data-translation-\<attributname\>-args" können wieder argumente 
für die Übersetzung angegeben werden. 

Der Client nutzt aus der .env-Datei folgende Variablen beim Builden (`cordova prepare`):
- `HOST` (Default: IP des Build-Computers, Adresse des Servers, inklusive http/https, aber ohne Port)
- `REQUEST_PORT` (Default: `PORT` (Falls `PORT` nicht gesetzt: 3000), Port auf dem der Client den Server versucht zu erreichen) 
- `MODE` (Default: "development", Für Production auf "production" stellen)