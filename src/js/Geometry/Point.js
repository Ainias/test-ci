import {Helper} from "js-helper/dist/shared/Helper";

export class Point {

    constructor(x, y, z) {
        this.x = Helper.nonNull(x, 0);
        this.y = Helper.nonNull(y, 0);
        this.z = Helper.nonNull(z, 0);
    }

    copy() {
        return new Point(this.x, this.y, this.z);
    }

    multiply(factorOrPoint) {

        let factorX = null;
        let factorY = null;
        let factorZ = null;
        if (factorOrPoint instanceof Point) {
            factorX = factorOrPoint.x;
            factorY = factorOrPoint.y;
            factorZ = factorOrPoint.z;
        } else {
            factorX = factorOrPoint;
            factorY = factorOrPoint;
            factorZ = factorOrPoint;
        }

        this.x *= factorX;
        this.y *= factorY;
        this.z *= factorZ;

        return this;
    }

    divide(dividerOrPoint) {

        let dividerX = null;
        let dividerY = null;
        let dividerZ = null;
        if (dividerOrPoint instanceof Point) {
            dividerX = dividerOrPoint.x;
            dividerY = dividerOrPoint.y;
            dividerZ = dividerOrPoint.z;
        } else {
            dividerX = dividerOrPoint;
            dividerY = dividerOrPoint;
            dividerZ = dividerOrPoint;
        }

        this.x /= dividerX;
        this.y /= dividerY;
        this.z /= dividerZ;

        return this;
    }

    intval() {
        this.x = parseInt(this.x);
        this.y = parseInt(this.y);
        this.z = parseInt(this.z);

        return this;
    }

    round() {
        this.x = Math.round(this.x);
        this.y = Math.round(this.y);
        this.z = Math.round(this.z);

        return this;
    }

    add(xOrPoint, y, z) {
        if (xOrPoint instanceof Point) {
            y = xOrPoint.y;
            z = xOrPoint.z;
            xOrPoint = xOrPoint.x;
        }

        this.x += xOrPoint;
        this.y += y;
        this.z += z;

        return this;
    }

    addX(x) {
        if (x instanceof Point) {
            x = x.x;
        }
        this.x += x;
        return this;
    }

    addY(y) {
        if (y instanceof Point) {
            y = y.y;
        }
        this.y += y;
        return this;
    }

    addZ(z) {
        if (z instanceof Point) {
            z = z.z;
        }
        this.z += z;
        return this;
    }

    substract(xOrPoint, y, z) {
        if (xOrPoint instanceof Point) {
            y = xOrPoint.y;
            z = xOrPoint.z;
            xOrPoint = xOrPoint.x;
        }

        this.x -= xOrPoint;
        this.y -= y;
        this.z -= z;

        return this;
    }

    substractX(x) {
        if (x instanceof Point) {
            x = x.x;
        }
        this.x -= x;
        return this;
    }

    substractY(y) {
        if (y instanceof Point) {
            y = y.y;
        }
        this.y -= y;
        return this;
    }

    substractZ(z) {
        if (z instanceof Point) {
            z = z.z;
        }
        this.z -= z;
        return this;
    }

    set(xOrPoint, y, z) {
        if (xOrPoint instanceof Point) {
            y = xOrPoint.y;
            z = xOrPoint.z;
            xOrPoint = xOrPoint.x;
        }

        this.x = xOrPoint;
        this.y = y;
        this.z = z;

        return this;
    }

    setX(x) {
        this.x = x;
        return this;
    }

    setY(y) {
        this.y = y;
        return this;
    }

    setZ(z) {
        this.z = z;
        return this;
    }

    smallerValuesThan(other) {
        return this.x < other.x && this.y < other.y && this.z < other.z;
    }

    smallerEqualValuesThan(other) {
        return this.x <= other.x && this.y <= other.y && this.z <= other.z;
    }

    greaterValuesThan(other) {
        return this.x > other.x && this.y > other.y && this.z > other.z;
    }

    greaterEqualValuesThan(other) {
        return this.x >= other.x && this.y >= other.y && this.z >= other.z;
    }

    smallerXThan(xOrOther) {
        if (xOrOther instanceof Point) {
            xOrOther = xOrOther.x;
        }
        return this.x < xOrOther;
    }

    smallerYThan(yOrOther) {
        if (yOrOther instanceof Point) {
            yOrOther = yOrOther.y;
        }
        return this.y < yOrOther;
    }

    smallerZThan(zOrOther) {
        if (zOrOther instanceof Point) {
            zOrOther = zOrOther.z;
        }
        return this.z < zOrOther;
    }

    greaterXThan(xOrOther) {
        if (xOrOther instanceof Point) {
            xOrOther = xOrOther.x;
        }
        return this.x > xOrOther;
    }

    greaterYThan(yOrOther) {
        if (yOrOther instanceof Point) {
            yOrOther = yOrOther.y;
        }
        return this.y > yOrOther;
    }

    greaterZThan(zOrOther) {
        if (zOrOther instanceof Point) {
            zOrOther = zOrOther.z;
        }
        return this.z > zOrOther;
    }

    smallerEqualXThan(xOrOther) {
        return !this.greaterXThan(xOrOther);
    }

    smallerEqualYThan(yOrOther) {
        return !this.greaterYThan(yOrOther);
    }

    smallerEqualZThan(zOrOther) {
        return !this.greaterZThan(zOrOther);
    }

    greaterEqualXThan(xOrOther) {
        return !this.smallerXThan(xOrOther);
    }

    greaterEqualYThan(yOrOther) {
        return !this.smallerYThan(yOrOther);
    }

    greaterEqualZThan(zOrOther) {
        return !this.smallerZThan(zOrOther);
    }

    equals(other) {
        return this.x === other.x
            && this.y === other.y
            && this.z === other.z
    }

    static max(p1, p2) {
        return new Point(
            Math.max(p1.x, p2.x),
            Math.max(p1.y, p2.y),
            Math.max(p1.z, p2.z)
        )
    }

    static min(p1, p2) {
        return new Point(
            Math.min(p1.x, p2.x),
            Math.min(p1.y, p2.y),
            Math.min(p1.z, p2.z)
        )
    }
}