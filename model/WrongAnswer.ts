import {BaseDatabase} from "cordova-sites-database/dist/cordova-sites-database";
import {UserSyncPartialModel} from "cordova-sites-user-management/dist/shared";
import {ExerciseProgress} from "./ExerciseProgress";

export class WrongAnswer extends UserSyncPartialModel {

    field: string;
    given: string;
    expected: string;

    occurredAt: any;
    constructor() {
        super();

        this.field = null;
        this.given = null;
        this.expected = null;
        this.occurredAt = new Date();
    }

    static getColumnDefinitions() {
        let columns = super.getColumnDefinitions();
        columns["field"] = BaseDatabase.TYPES.STRING;
        columns["given"] = {type: BaseDatabase.TYPES.STRING, nullable: true};
        columns["expected"] = BaseDatabase.TYPES.STRING;
        columns["occurredAt"] = BaseDatabase.TYPES.DATE;
        return columns;
    }

    static getRelationDefinitions() {
        let relations = super.getRelationDefinitions();
        relations["exerciseProgress"] = {
            target: ExerciseProgress.getSchemaName(),
            type: "many-to-one",
            joinColumn: true,
        };
        return relations;
    }
}

BaseDatabase.addModel(WrongAnswer);