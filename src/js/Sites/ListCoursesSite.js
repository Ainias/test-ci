import view from "../../html/sites/listCoursesSite.html";
import {LexiconSite} from "./LexiconSite";
import {SelectExerciseDialog} from "../Dialog/SelectExerciseDialog";
import {Helper, MenuAction, MenuSite} from "cordova-sites/dist/client";
import {UserManager, UserMenuAction} from "cordova-sites-user-management/dist/client";
import {ListExerciseSite} from "./ListExerciseSite";
import {Course} from "../../../model/Course.ts";
import {EditExerciseSite} from "./EditExerciseSite";

import holzbauJPG from "../../img/courses/Holzbau.jpg"
import kanalbauJPG from "../../img/courses/Kanalbau.JPG"
import mauerwerksbauJPG from "../../img/courses/Mauerwerksbau.JPG"
import rohrleitungsbauJPG from "../../img/courses/Rohrleitungsbau.JPG"
import stahlbetonbauJPG from "../../img/courses/Stahlbetonbau.jpg"
import strassenbauJPG from "../../img/courses/Straßenbau.jpg"
import tiefbauJPG from "../../img/courses/Tiefbau.JPG"
import {StatisticsSite} from "./StatisticsSite";
import {StartSiteMenuAction} from "cordova-sites/dist/client/js/Context/Menu/MenuAction/StartSiteMenuAction";
import {ListWikiEntries} from "./ListWikiEntries";
import {EditStorybookSite} from "./EditStorybookSite";

export class ListCoursesSite extends MenuSite {
    constructor(siteManager) {
        super(siteManager, view);
        this._navbarFragment.setCanGoBack(false);
    }

    async onViewLoaded() {

        this._courseTemplate = this.findBy("#course-template");
        this._courseTemplate.remove();
        this._courseTemplate.removeAttribute("id");

        this._courseContainer = this.findBy("#course-container");

        return super.onViewLoaded();
    }

    onCreateMenu(navbar) {
        navbar.addAction(new UserMenuAction("edit-article-site", "admin", async () => {
            let type = await new SelectExerciseDialog().show();
            if (typeof type === "string") {
                if (type === "storybook") {
                    this.startSite(EditStorybookSite, {courseId: 1});
                } else {
                    this.startSite(EditExerciseSite, {type: type, courseId: 1});
                }
            }
        }));

        navbar.addAction(new MenuAction("lexicon-site", () => {
            this.startSite(LexiconSite);
        }));

        navbar.addAction(new UserMenuAction("statistics", "admin", () => {
            this.startSite(StatisticsSite);
        }));

        navbar.addAction(new StartSiteMenuAction("Wiki", ListWikiEntries))
    }

    async onStart(pauseArguments) {
        await super.onStart(pauseArguments);

        let courses = await Course.select(null, {"id": "desc"});
        Helper.removeAllChildren(this._courseContainer);
        courses.forEach(course => this._addCourse(course));
    }

    _editSource(source) {
        if (source.endsWith("Holzbau.jpg")) {
            return holzbauJPG;
        }
        if (source.endsWith("Kanalbau.JPG")) {
            return kanalbauJPG;
        }
        if (source.endsWith("Mauerwerksbau.JPG")) {
            return mauerwerksbauJPG;
        }
        if (source.endsWith("Rohrleitungsbau.JPG")) {
            return rohrleitungsbauJPG;
        }
        if (source.endsWith("Stahlbetonbau.jpg")) {
            return stahlbetonbauJPG;
        }
        if (source.endsWith("Straßenbau.jpg")) {
            return strassenbauJPG;
        }
        if (source.endsWith("Tiefbau.JPG")) {
            return tiefbauJPG;
        }
        return source;
    }

    /**
     *
     * @param {Course} course
     * @private
     */
    _addCourse(course) {
        let courseElement = this._courseTemplate.cloneNode(true);
        let name = courseElement.querySelector(".name");

        name.appendChild(document.createTextNode(course.name));
        courseElement.querySelector(".icon").src = this._editSource(course.icon);

        courseElement.addEventListener("click", async () => {
            if (course.activated || await UserManager.getInstance().hasAccess("admin")) {
                this.startSite(ListExerciseSite, {courseId: course.id})
            }
        });

        if (course.activated) {
            courseElement.classList.add("activated");
        }

        this._courseContainer.appendChild(courseElement);
    }
}