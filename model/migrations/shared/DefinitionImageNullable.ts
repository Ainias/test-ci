import {MigrationInterface, QueryRunner, Table} from "typeorm";
import {BaseDatabase} from "cordova-sites-database/dist/BaseDatabase";

export class DefinitionImageNullable1000000005000 implements MigrationInterface {

    _isServer(): boolean {
        return (typeof document !== "object")
    }

    async up(queryRunner: QueryRunner): Promise<any> {
        if (this._isServer()) {
            await this.modifyOnServer(queryRunner);
        }
        else {
            await this.modifyOnClient(queryRunner);
        }
    }

    private async modifyOnServer(queryRunner: QueryRunner) {
        await queryRunner.query("ALTER TABLE definition MODIFY COLUMN image MEDIUMTEXT NULL;");
    }

    private async modifyOnClient(queryRunner: QueryRunner) {
        let definition = new Table({
            name: "definition_new",
            columns: [
                {
                    name: "id",
                    isPrimary: true,
                    type: BaseDatabase.TYPES.INTEGER,
                    isGenerated: this._isServer(),
                    generationStrategy: "increment" as "increment"
                },
                {
                    name: "createdAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "updatedAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "version",
                    type: BaseDatabase.TYPES.INTEGER,
                },
                {
                    name: "deleted",
                    type: BaseDatabase.TYPES.BOOLEAN,
                },
                {
                    name: "userId",
                    type: BaseDatabase.TYPES.INTEGER,
                    isNullable: true
                },
                {
                    name: "key",
                    type: BaseDatabase.TYPES.STRING,
                    isUnique: true,
                },
                {
                    name: "keywords",
                    type: BaseDatabase.TYPES.TEXT,
                },
                {
                    name: "description",
                    type: BaseDatabase.TYPES.STRING,
                },
                {
                    name: "image",
                    type:  BaseDatabase.TYPES.TEXT,
                    isNullable: true
                },
                {
                    name: "imageDescription",
                    type: BaseDatabase.TYPES.STRING,
                    isNullable: true
                }
            ],
            indices: [
                {
                    name: "IDX_definition_userId_2",
                    columnNames: ["userId"]
                },
            ],
            foreignKeys: [
                {
                    name: "FK_definition_userId_2",
                    columnNames: ["userId"],
                    referencedTableName: "user",
                    referencedColumnNames: ["id"],
                },
            ]
        });
        await queryRunner.createTable(definition);

        await queryRunner.query("INSERT INTO definition_new SELECT id, createdAt, updatedAt, version, deleted, userId, key, keywords, description, image, imageDescription FROM definition;");
        await queryRunner.query("DROP TABLE definition;");
        await queryRunner.query("ALTER TABLE definition_new RENAME TO definition;");
    }

    down(queryRunner: QueryRunner): Promise<any> {
        return undefined;
    }
}