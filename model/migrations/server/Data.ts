import {MigrationInterface, QueryRunner} from "typeorm";
import {UserManager} from "cordova-sites-user-management/dist/server/v1/UserManager";
import {User} from "cordova-sites-user-management/dist/shared/v1/model/User";

export class Data1000000005000 implements MigrationInterface{
    async up(queryRunner: QueryRunner): Promise<any> {
        await this._insertAccesses(queryRunner);
        await this._insertRoles(queryRunner);
        await this._insertRoleChildren(queryRunner);
        await this._insertRoleAccess(queryRunner);
        await this._insertAdminUser(queryRunner);
        await this._insertCourse(queryRunner);
    }

    async _insertAccesses(queryRunner: QueryRunner){
        await queryRunner.query("INSERT INTO `access` VALUES " +
            "(1,'2019-06-04 16:51:17','2019-06-04 16:51:17',2,0,'default','everyone is allowed to do this!')," +
            "(2,'2019-06-04 16:51:17','2019-06-04 16:51:17',2,0,'offline','does not has internet access!')," +
            "(3,'2019-06-04 16:51:17','2019-06-04 16:51:17',2,0,'online','has internet access')," +
            "(4,'2019-06-04 16:51:17','2019-06-04 16:51:17',2,0,'loggedOut','for users, that are not logged in')," +
            "(5,'2019-06-04 16:51:17','2019-06-04 16:51:17',2,0,'loggedIn','for users, that are logged in')," +
            "(6,'2019-06-04 16:51:17','2019-06-04 16:51:17',2,0,'admin','Access for admins');");
    }

    async _insertRoles(queryRunner: QueryRunner){
        await queryRunner.query("INSERT INTO `role` VALUES " +
            "(1,'2019-06-04 16:51:17','2019-06-04 16:51:17',2,0,'offlineRole','role for user that are offline')," +
            "(2,'2019-06-04 16:51:17','2019-06-04 16:51:17',2,0,'onlineRole','role for user that are online')," +
            "(3,'2019-06-04 16:51:17','2019-06-04 16:51:17',2,0,'visitorRole','role for user that are online, but not logged in')," +
            "(4,'2019-06-04 16:51:17','2019-06-04 16:51:17',2,0,'memberRole','role for user that are online and logged in')," +
            "(5,'2019-06-04 16:51:18','2019-06-04 16:51:18',2,0,'Admin','Role for Admins (online, logged in and admin)');");
    }

    async _insertRoleChildren(queryRunner: QueryRunner){
        await queryRunner.query("INSERT INTO `roleChildren` (childId, parentId) VALUES " +
            "(3,2)," +
            "(4,2)," +
            "(5,4)");
    }

    async _insertRoleAccess(queryRunner: QueryRunner){
        await queryRunner.query("INSERT INTO `roleAccess` (roleId, accessId) VALUES " +
            "(1,1)," +
            "(1,2)," +
            "(2,1)," +
            "(2,3)," +
            "(3,4)," +
            "(4,5)," +
            "(5,6)");
    }

    async _insertAdminUser(queryRunner: QueryRunner){
        let pw = "123456";
        let user = new User();
        pw = UserManager._hashPassword(user, pw);
        let salt = user.salt;

        await queryRunner.query("INSERT INTO `user` VALUES " +
            "(1,'2019-06-04 16:51:18','2019-06-04 16:51:24',3,0,'admin','admin@mbb.de','"+pw+"',1,0,'"+salt+"')")
        await queryRunner.query("INSERT INTO `userRole` VALUES (1,5)");
        await queryRunner.query("INSERT INTO `user_access` VALUES " +
            "(1,1,6)," +
            "(2,1,5)," +
            "(3,1,1)," +
            "(4,1,3)");
    }

    async _insertCourse(queryRunner: QueryRunner){
        await queryRunner.query("INSERT INTO `course` VALUES " +
            "(1,'2019-06-04 16:51:19','2019-09-10 16:51:19',1,0,'Mauerwerksbau',0,0,'/img/courses/Mauerwerksbau.JPG')," +
            "(2,'2019-06-04 16:51:20','2019-09-10 12:05:48',1,0,'Holzbau',0,0,'/img/courses/Holzbau.jpg')," +
            "(3,'2019-06-04 16:51:23','2019-09-10 12:41:08',1,0,'Rohrleitungsbau',0,0,'/img/courses/Rohrleitungsbau.JPG')," +
            "(4,'2019-06-04 16:51:24','2019-09-10 14:00:01',1,0,'Kanalbau',0,0,'/img/courses/Kanalbau.JPG')," +
            "(5,'2019-06-04 16:51:24','2019-09-10 12:22:39',1,0,'Stahlbetonbau',0,0,'/img/courses/Stahlbetonbau.jpg')," +
            "(6,'2019-06-04 16:51:24','2019-09-10 16:51:24',1,0,'Straßenbau',0,0,'/img/courses/Straßenbau.jpg')," +
            "(7,'2019-06-04 16:51:24','2019-09-10 16:51:24',1,0,'Tiefbau',0,0,'/img/courses/Tiefbau.JPG');");
    }

    down(queryRunner: QueryRunner): Promise<any> {
        return undefined;
    }
}