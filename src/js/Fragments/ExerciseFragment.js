import view from "../../html/Fragments/exerciseFragment.html";
import {PictureFragment} from "./ExerciseFragments/Views/PictureFragment";
import {TextFragment} from "./ExerciseFragments/Views/TextFragment";
import {WallLengthExerciseFragment} from "./ExerciseFragments/WallLengthExerciseFragment";
import {AbstractFragment, Helper} from "cordova-sites/dist/client";
import {VideoFragment} from "./ExerciseFragments/Views/VideoFragment";
import {BuildWallExerciseFragment} from "./ExerciseFragments/BuildWallExerciseFragment";
import {WallLengthEightExerciseFragment} from "./ExerciseFragments/WallLengthEightExerciseFragment";
import {BuildWallDimension} from "./ExerciseFragments/BuildWallDimension";
import {Generator} from "../ExerciseGenerator/Generator";
import {EditExerciseSite} from "../Sites/EditExerciseSite";
import {ExerciseHelper} from "../Helpers/ExerciseHelper";
import {StorybookFragment} from "./ExerciseFragments/StorybookFragment";

export class ExerciseFragment extends AbstractFragment {

    constructor(site) {
        super(site, view);
        this._mainFragment = null;
    }

    async setExercise(exercise) {
        this._exercise = exercise;

        let progress = await ExerciseHelper.getProgressFor(this._exercise);

        if (progress.data === null){
            if (this._exercise.isGenerating){
                let generator = new Generator();
                let {validator, values} = EditExerciseSite._getTypeInfos(this._exercise.elementType);
                let data = await generator.generateData(this._exercise.generatingData, validator);

                await Helper.asyncForEach(Object.keys(values), async value => {
                    if (typeof values[value] === "number"){
                        values[value] = {
                            type: values[value]
                        }
                    }
                    if (values[value].type === EditExerciseSite.TYPE.EXERCISE_IMAGE){
                        data[value] = await generator._generateImageFor(this._exercise.elementType, data);
                    }
                }, true);

                progress.data = data;
            }
            else {
                progress.data = this._exercise.generatingData;
            }
        }

        //await view for name and view-container!
        await this._viewPromise;
        this.findBy("#article-name").innerHTML = this._exercise.getName();

        await this.addFragmentForElement(this._exercise);

        //await vies from child-fragments
        await this._viewPromise;
        // debugger;
        // this._view = await Lexicon.getInstance().updateDefinitions(this._view);
        // debugger;
    }

    async onViewLoaded() {
        this.findBy("#back-button").addEventListener("click", () => {
            if (Helper.isNotNull(this._mainFragment) && "goBack" in this._mainFragment) {
                this._mainFragment.goBack();
            }
        });
        return await super.onViewLoaded();
    }

    async addFragmentForElement(element) {
        let newFragment = null;
        switch (element.getElementType()) {
            case "element-picture": {
                newFragment = new PictureFragment(this);
                break;
            }
            case "element-text": {
                newFragment = new TextFragment(this);
                break;
            }
            case "element-wall-length-exercise": {
                newFragment = new WallLengthExerciseFragment(this);
                break;
            }
            case "element-wall-length-eight-exercise": {
                newFragment = new WallLengthEightExerciseFragment(this);
                break;
            }
            //BUILD WALL DIMENSION
            case "element-build-wall-dimension": {
                newFragment = new BuildWallDimension(this);
                break;
            }
            //BUILD WALL
            case "element-build-wall-exercise": {
                newFragment = new BuildWallExerciseFragment(this);
                break;
            }
            case "element-video": {
                newFragment = new VideoFragment(this);
                break;
            }
            case "storybook": {
                newFragment = new StorybookFragment(this);
                break;
            }
            default: {
                console.error("not supported element type!", element);
                return;
            }
        }

        if (Helper.isNull(this._mainFragment)) {
            this._mainFragment = newFragment;
        }

        await newFragment.setElement(element);
        this.addFragment("#view-container", newFragment);
    }
}