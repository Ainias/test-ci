export class EventMessage{
    constructor(message, event){
        this._message = message;
        this._event = event;
        this._logDate = new Date();
    }
}