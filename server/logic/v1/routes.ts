import * as express from 'express';
import {userRoutes, syncRoutes, UserManager} from "cordova-sites-user-management/dist/server";
import {DefinitionController} from "./controller/DefinitionController";
import {Definition} from "../../../model/Definition";
import {StatisticController} from "./controller/StatisticController";
import {UserController} from "./controller/UserController";

const routerV1 = express.Router();

const errorHandler = (fn, context) => {
    return (req, res, next) => {
        const resPromise = fn.call(context, req,res,next);
        if (resPromise && resPromise.catch){
            resPromise.catch(err => next(err));
        }
    }
};

routerV1.use("/sync", syncRoutes);
routerV1.use("/user", userRoutes);

routerV1.post(Definition.SAVE_PATH, errorHandler(UserManager.setUserFromToken, UserManager), errorHandler(DefinitionController.modifyModel, DefinitionController));
routerV1.get("/statistics", errorHandler(UserManager.needToken, UserManager), errorHandler(StatisticController.getDefaultStatistic, StatisticController));
routerV1.get("/listUsers", errorHandler(UserManager.needToken, UserManager), errorHandler(UserController.listUsers, UserController));

export {routerV1};