import {Definition} from "../../model/Definition";
import {ShowDefinitionDialog} from "./Dialog/ShowDefinitionDialog";
import {App, Translator} from "cordova-sites/dist/client";
import {Helper} from "js-helper/dist/shared";
import {UserManager} from "cordova-sites-user-management/dist/client";
import {User} from "cordova-sites-user-management/dist/shared";
import {ViewHelper} from "js-helper/dist/client/ViewHelper";

export class Lexicon {
    constructor() {
        this._definitions = null;
        this._definitionKeys = {};
    }

    async updateDefinitions(element) {
        if (element.children.length === 0) {
            let preparedElement = await this.prepareText(element.innerHTML);
            ViewHelper.removeAllChildren(element);
            ViewHelper.moveChildren(preparedElement, element);
        } else {
            await Helper.asyncForEach(element.children, async child => {
                await this.updateDefinitions(child);
            }, true);
        }
        return element;
    }

    async _loadDefinitions(reload) {
        if (Helper.isNull(this._definitions) || reload) {

            let definitions = await Definition.find(null, {"key": "asc"});
            this._definitions = {};
            this._definitionKeys = {};
            definitions.forEach(definition => {
                this._definitions[definition.getId()] = definition;
                this._definitionKeys[definition.getKey()] = definition.getId();
            });
        }
    }

    async getDefinitions(sorted, reload) {
        await this._loadDefinitions(reload);
        let result = Helper.toArray(this._definitions);
        if (Helper.nonNull(sorted, true)) {
            result.sort((a, b) => {
                return (a.getKey().toLowerCase() < b.getKey().toLowerCase()) ? -1 : 1;
            });
        }
        return result;
    }

    async addOrUpdateDefinition(definition, forMeOnly) {
        forMeOnly = Helper.nonNull(forMeOnly, true);

        if (definition instanceof Definition) {
            await this._loadDefinitions();
            if (Helper.isNull(this._definitions[this._definitionKeys[definition.getKey()]]) ||
                (Helper.isNotNull(definition.getId()) && this._definitions[definition.getId()].getId() === definition.getId())) {
                if (forMeOnly) {
                    definition.userId = UserManager.getInstance().getUserData().id;
                    let user = new User();
                    user.id = definition.userId;
                    definition.user = user;
                }
                await definition.save();
                this._definitions[definition.getId()] = definition;
                this._definitionKeys[definition.getKey()] = definition.getId();
            }
        }
    }

    async prepareText(text, ignoredKey) {
        if (text.trim() === "") {
            return document.createTextNode(text);
        }
        await this._loadDefinitions();
        Helper.toArray(this._definitions).forEach(definition => {
            if (definition.getKey() !== ignoredKey) {
                let regexs = definition.getRegexs();
                regexs.forEach(regex => {
                    text = text.replace(regex, "<span class = 'definition-link' data-definition-id='" + definition.getId() + "' >$1</span>");
                });
            }
        });
        let span = document.createElement("span");
        span.innerHTML = text;

        span.querySelectorAll("span.definition-link").forEach(definitionElement => {
            definitionElement.addEventListener("click", (e) => {
                if (Helper.isNotNull(this._definitions[definitionElement.dataset["definitionId"]])) {
                    new ShowDefinitionDialog(this._definitions[definitionElement.dataset["definitionId"]]).show();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
        });

        return span;
    }

    /**
     *
     * @return {Lexicon}
     */
    static getInstance() {
        if (Helper.isNull(this._instance)) {
            this._instance = new Lexicon();
        }
        return this._instance;
    };
}

App.addInitialization((app) => {
    app.ready(() => {
        Translator.addTranslationCallback(async (baseElement) => {
            if (typeof baseElement !== "undefined") {
                await Lexicon.getInstance().updateDefinitions(baseElement);
            }
        })
    });
});

Lexicon._instance = null;