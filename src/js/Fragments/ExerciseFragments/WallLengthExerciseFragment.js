import {ExerciseDescriptionFragment} from "./SubFragments/ExerciseDescriptionFragment";
import {CalculationFragment} from "./SubFragments/CalculationFragment/CalculationFragment";
import {SwipeFragment} from "cordova-sites/dist/client";
import {ExerciseHelper} from "../../Helpers/ExerciseHelper";

export class WallLengthExerciseFragment extends SwipeFragment {
    constructor(site) {
        super(site);

        this._exerciseDescriptionFragment = new ExerciseDescriptionFragment(this.getSite());
        this._calculationFragment = new CalculationFragment(this.getSite(), WallLengthExerciseFragment.STONE_LENGTHS);

        this._element = null;

        this.addFragment(this._exerciseDescriptionFragment);
        this.addFragment(this._calculationFragment);
    }

    async setElement(element){
        this._element = element;

        let progress = await ExerciseHelper.getProgressFor(element);

        let data = progress.getData();
        Object.keys(data).forEach(key => {
            if (key.startsWith("exercise-num")){
                data[key.substr(9)] = data[key];
            }
        });

        data["num-joint"] = parseInt(data["num-whole-stone"])
            + parseInt(data["num-three-quarter-stone"])
            + parseInt(data["num-half-stone"])
            + parseInt(data["num-quarter-stone"])
            + parseInt(data["num-stones-sideways"])
            - 1;

        data["exercise-understood-exercise-question"] = "Welchen Wert sollst du berechnen?";
        data["exercise-understood-exercise-answer-1"] = "Länge der Mauer";
        data["exercise-understood-exercise-answer-2"] = "Höhe der Mauer";
        data["exercise-understood-exercise-answer-3"] = "Anzahl der Steine";
        data["exercise-understood-exercise-answer-4"] = "Breite der Mauer";
        data["exercise-understood-exercise-correct"] = "1";

        progress.setData(data);

        await this._exerciseDescriptionFragment.setElement(element);
        await this._calculationFragment.setElement(element);
    }

    goBack(){
        return this.previousFragment();
    }

    previousFragment() {
        if (this._activeIndex > 0) {
            super.previousFragment();
        } else {
            this.getSite().goBack();
        }
    }
}

WallLengthExerciseFragment.STONE_LENGTHS = {
    "whole-stone": 24,
    "three-quarter-stone": 17.75,
    "half-stone": 11.5,
    "quarter-stone": 5.25,
    "stones-sideways":11.5,
    "joint": 1
};