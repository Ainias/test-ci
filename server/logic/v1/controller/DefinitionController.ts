import {EasySyncController, EasySyncServerDb} from "cordova-sites-easy-sync/dist/server";
import {Definition} from "../../../../model/Definition";
import {UserManager} from "cordova-sites-user-management/dist/server";
import {Helper} from "js-helper/dist/shared";

export class DefinitionController extends EasySyncController {
    static async modifyModel(req, res) {

        debugger;
        let modelName = req.body.model;
        let modelData = req.body.values;

        let model = EasySyncServerDb.getModel(modelName);
        if (model !== Definition) {
            throw new Error("tried to modify model " + model.getSchemaName() + " which is not Definition!");
        }

        if (!Array.isArray(modelData)){
            modelData = [modelData];
        }

        let entities = await model._fromJson(modelData, undefined, true);

        let user = req.user;
        if (entities.some(definition => definition.userId !== user.id && Helper.isNotNull(definition.userId))) {
            throw new Error("tried to modify Definition with wrong userId!");
        }
        if (!await UserManager.hasAccess(user, "admin")) {
            //If existing entity for everyone
            if (entities.some(definition => Helper.isNull(definition.userId) && Helper.isNotNull(definition.id))) {
                throw new Error("tried to modify Definition without userId without the proper rights!");
            }
        }

        return res.json(await this._doModifyModel(model, entities));
    }
}