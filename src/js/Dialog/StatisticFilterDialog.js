import {Dialog} from "cordova-sites/dist/client/js/Dialog/Dialog";
import {ViewInflater} from "cordova-sites/dist/client/js/ViewInflater";

import view from "../../html/Dialog/statisticFilterDialog.html";
import {Form} from "cordova-sites/dist/client/js/Form";
import {SelectUserDialog} from "cordova-sites-user-management/dist/client";
import {Helper} from "js-helper/dist/shared/Helper";

import datepicker from "js-datepicker";
import {DateHelper} from "js-helper/dist/shared/DateHelper";

export class StatisticFilterDialog extends Dialog {

    constructor(values) {
        super(ViewInflater.getInstance().load(view).then(async view => {
            this._form = new Form(view.querySelector("#filter-form"));
            await this._form.setValues(values);
            view.querySelector("#user-selection").addEventListener("click", async () => {
                this._setUser(await (new SelectUserDialog().show()));
            });

            this._usernameElement = view.querySelector("#username");
            this._userIdElement = view.querySelector("#user-id");
            view.querySelector("#remove-user-button").addEventListener("click", (e) => {
                this._setUser({id: "", username: ""});
                e.stopPropagation();
            });

            let formatter = (input, date) => {
                input.value = DateHelper.strftime("%Y-%m-%d", date);
            };

            console.log("values", values, new Date(values["start"]));
            this._datepickerStart = datepicker(view.querySelector("[name=start]"), {
                id: 1,
                formatter: formatter,
                startDate: new Date(values["start"]),
                // onSelect: () => datepickerStart.hide(),
                // alwaysShow: false,
            });
            this._datepickerEnd = datepicker(view.querySelector("[name=end]"), {
                id: 1,
                formatter: formatter,
                startDate: (Helper.isNotNull(values["end"]) && values["end"].trim() !== "" ? new Date(values["end"]) : null),
                // onSelect: instance => instance.hide(),
                // alwaysShow: false,
            });

            return view;
        }), "filter");

        this.addButton("Save", async () => {
            this._result = await this._form.getValues();
            this.close();
        }, false)
    }

    close() {
        this._datepickerStart.remove();
        this._datepickerEnd.remove();
        super.close();
    }

    _setUser(user) {
        if (Helper.isNotNull(user)) {
            this._usernameElement.value = user.username;
            this._userIdElement.value = user.id;
        }
    }
}