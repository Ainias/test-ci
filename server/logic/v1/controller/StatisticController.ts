import {StatisticQueryBuilder} from "../../../../shared/statisticQuery/StatisticQueryBuilder";
import {EasySyncServerDb} from "cordova-sites-easy-sync/dist/server";
import {UserManager} from "cordova-sites-user-management/dist/server";
import {Helper} from "js-helper/dist/shared";

export class StatisticController {

    static async getDefaultStatistic(req, res) {
        if (!await UserManager.hasAccess(req.user, "admin")) {
            throw new Error("no rights for statistic!");
        }

        let userId = req.query.userId;
        let queryBuilder = await EasySyncServerDb.getInstance().createQueryBuilder();

        let start = req.query.start;
        let end = req.query.end;
        let exerciseId = req.query.exerciseId;

        let data = await StatisticQueryBuilder.getSummedData(queryBuilder, start, end, userId, exerciseId);

        data["time-exercises-done"] = Helper.nonNull(data["time-exercises-done"], 0);
        data["time-exercises-aborted"] = Helper.nonNull(data["time-exercises-aborted"], 0);

        res.json({
            success: true,
            data: data
        });
    }
}