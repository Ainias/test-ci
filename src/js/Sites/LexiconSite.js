import view from "../../html/sites/lexiconSite.html";
import {Lexicon} from "../Lexicon";
import {AddDefinitionDialog} from "../Dialog/AddDefinitionDialog";
import {App, Helper, MenuSite, Translator} from "cordova-sites/dist/client";
import {UserMenuAction} from "cordova-sites-user-management/dist/client/js/MenuAction/UserMenuAction";

export class LexiconSite extends MenuSite{
    constructor(siteManager) {
        super(siteManager, view);
    }

    onCreateMenu(navbar) {
        navbar.addAction(new UserMenuAction("add-definition", "loggedIn", async () => {
            await new AddDefinitionDialog().show();
            await this.addDefinitions();
        }));

        super.onCreateMenu(navbar);
    }

    async onViewLoaded() {
        this._definitionTemplate = this.findBy(".definition");
        this._definitionTemplate.remove();

        this._definitionContainer = this.findBy("#definition-container");

        await this.addDefinitions();
        return super.onViewLoaded();
    }

    async addDefinitions(){
        Helper.removeAllChildren(this._definitionContainer);
        let definitions = await Lexicon.getInstance().getDefinitions(true, true);
        await Helper.asyncForEach(definitions,async definition => {
            await this._addDefinitionElement(definition);
        });
        if (definitions.length === 0){
            this._definitionContainer.appendChild(Translator.getInstance().makePersistentTranslation("no definitions"))
        }
    }

    async _addDefinitionElement(definition){

        let definitionElement = this._definitionTemplate.cloneNode(true);

        this._definitionContainer.appendChild(definitionElement);
        definitionElement.querySelector(".key").appendChild(document.createTextNode(definition.getKey()));
        definitionElement.querySelector(".keywords").appendChild(document.createTextNode(definition.getKeywords().join(", ")));
        definitionElement.querySelector(".description").appendChild(await Lexicon.getInstance().prepareText(definition.getDescription(), definition.getKey()));

        let img = definition.getImage();
        if (Helper.isNotNull(img)){
            definitionElement.querySelector(".image-container").classList.remove("hidden");
            definitionElement.querySelector(".image").setAttribute("src", img);
            definitionElement.querySelector(".image-description").appendChild(await Lexicon.getInstance().prepareText(definition.getImageDescription(), definition.getKey()));
        }
    }
}

App.addInitialization((app) => {
    app.addDeepLink("lexicon", LexiconSite);
});