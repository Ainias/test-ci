import "dotenv/config";
import {MysqlConnectionOptions} from "typeorm/driver/mysql/MysqlConnectionOptions";
import {SetupUserManagement1566377719953} from "cordova-sites-user-management/src/migrations/SetupUserManagement"
import {Setup1566377719955} from "./model/migrations/shared/Setup";
import {Data1566377719956} from "./model/migrations/server/Data";

const config: MysqlConnectionOptions = {
    type: 'mysql',
    host: process.env.MYSQL_HOST || "localhost",
    port: parseInt(process.env.MYSQL_PORT || "3306"),
    username: process.env.MYSQL_USER || "root",
    password: process.env.MYSQL_PASSWORD || "",
    database: process.env.MYSQL_DATABASE || "mbb_test",
    synchronize: false,
    logging: true,
    migrations: [
        SetupUserManagement1566377719953,
        Setup1566377719955,
        Data1566377719956
    ]
};

export = config;