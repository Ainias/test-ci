import {Dialog} from "cordova-sites/dist/client/js/Dialog/Dialog";

import view from "../../html/Dialog/saveStorybookDialog.html";
import {ViewInflater} from "cordova-sites/dist/client/js/ViewInflater";
import {Form} from "cordova-sites/dist/client/js/Form";
import {Course} from "../../../model/Course";
import {Helper} from "cordova-sites/dist/client";

export class SaveStorybookDialog extends Dialog {

    constructor(name, selectedCourseId) {
        super(ViewInflater.getInstance().load(view).then(async view => {
            let courses = await Course.find(undefined, {"id": "ASC"});
            courses = Helper.arrayToObject(courses, course => course.id);

            let selectCourseInput = view.querySelector("#article-course");
            Object.keys(courses).forEach(courseId => {
                let option = document.createElement("option");
                option.value = courseId;
                option.innerText = courses[courseId].name;

                if (parseInt(courseId) === selectedCourseId) {
                    option.selected = true;
                }
                selectCourseInput.appendChild(option);
            });

            let form = new Form(view.querySelector("#save-storybook-form"), values => {
                this._result = values;
                this.close();
            });

            let values = {};
            values["name"] = name;
            values["course"] = selectedCourseId;
            form.setValues(values);

            return view;
        }), "save storybook");
    }
}