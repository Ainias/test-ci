import view from "../../html/Dialog/addDefinitionDialog.html";
import {Definition} from "../../../model/Definition.ts";
import {Lexicon} from "../Lexicon";
import {Dialog, Form, Helper, Toast} from "cordova-sites/dist/client";
import {UserManager} from "cordova-sites-user-management/dist/client";

export class AddDefinitionDialog extends Dialog{

    constructor(definition) {
        super(view, "add-definition-dialog");
        this._definition = Helper.nonNull(definition, new Definition());
    }

    async setContent(content) {
        let res = super.setContent(content);
        res = await res;

        this._form = new Form(this._content.querySelector(".definition-input-form"), async(values) => {
            let keywords = values["keywords"].split(",");
            keywords.forEach((keyword, index) => {
                keywords[index] = keyword.trim();
            });
            this._definition.setKeywords(keywords);
            this._definition.setKey(values["key"].trim());
            this._definition.setDescription(values["description"].trim());

            if (values["image"] && values["image"].trim() !== "" && values["image"].trim() !== "data:"){
                this._definition.setImage(values["image"]);
                this._definition.setImageDescription(values["image-description"]);
            }
            else {
                this._definition.setImage(null);
                this._definition.setImageDescription(null);
            }

            let saveForAll = await UserManager.getInstance().hasAccess("admin") && Helper.isNotNull(values["save-for-all"]);

            try {
                await Lexicon.getInstance().addOrUpdateDefinition(this._definition, !saveForAll);
                this._result = this._definition;
                this.close()
            }
            catch(e){
                new Toast("something went wrong...").show();
                console.error(e);
            }

        });

        this._form.addValidator(values => {
            if (values["image"] && !Helper.imageUrlIsEmpty(values["image"]) && values["image-description"].trim() === ""){
                return {
                    "image-description":"image-description must be set, if image is set",
                }
            }
            return true;
        });

        this._form.addValidator(async values => {
            if (await Definition.findOne({"key": values["key"].trim()})){
                return {
                    "key":"key already exists!",
                }
            }
            return true;
        });

        return res;
    }
}