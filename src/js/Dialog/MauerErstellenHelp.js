import {Dialog, ViewInflater} from "cordova-sites/dist/client";

import view from "../../html/Dialog/MauerErstellenHelp.html";

export class MauerErstellenHelp extends Dialog {

    constructor(){
     super(view,"Help");
    }
}

