import {ModifyEntitySite} from "cordova-sites-easy-sync/dist/client/editEntitySite/ModifyEntitySite";
import {WikiEntry} from "../../../model/WikiEntry";

import view from "../../html/sites/editWikiEntry.html";
import {UserSite} from "cordova-sites-user-management/dist/client/js/Context/UserSite";

export class EditWikiEntrySite extends ModifyEntitySite{

    constructor(siteManager) {
        super(siteManager, view, WikiEntry);
        this.addDelegate(new UserSite(this, "admin"));
    }
}