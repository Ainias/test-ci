import {Dialog, ViewInflater} from "cordova-sites/dist/client";

import view from "../../html/Dialog/calculator.html";
import * as mathjs from 'mathjs';

export class Calculator extends Dialog {

    constructor() {
        let viewPromise = ViewInflater.getInstance().load(view).then(view => {
            this._inputElement = view.querySelector(".equation");
            this._outputElement = view.querySelector(".answer");

            this._inputElement.addEventListener("keyup", e => {
                if (e.code === "Enter" || e.code === "NumpadEnter") {
                    this.evaluateEquation(this._inputElement.innerText);
                } else {
                    console.log(e);
                }
            });
            view.querySelector(".result-button").addEventListener("click", () => {
                this.evaluateEquation(this._inputElement.innerText);
            });
            view.querySelectorAll("[data-value]").forEach(element => {
                element.addEventListener("click", e => {
                    this.insertValue(e.target.dataset["value"]);
                })
            });
            let deleteButton = view.querySelector(".delete-input");
            deleteButton.addEventListener("mousedown", () => {
                this._inputElement.innerText = this._inputElement.innerText.substr(0, this._inputElement.innerText.length-1);
                this._mouseDownTimer = setTimeout(() => {
                    this._outputElement.innerText = "";
                    this._inputElement.innerText = "";

                }, Calculator.TIME_LONG_PRESS);
            });
            deleteButton.addEventListener("mouseup", () => {
                clearTimeout(this._mouseDownTimer);
            });

            this._inputElement.focus();
            return view;
        });
        super(viewPromise, "calculator");
    }

    insertValue(value) {
        if (document.selection) {
            // IE
            this._inputElement.focus();
            let sel = document.selection.createRange();
            sel.text = value;
        } else if (this._inputElement.selectionStart || this._inputElement.selectionStart === 0) {
            // Others
            let startPos = this._inputElement.selectionStart;
            let endPos = this._inputElement.selectionEnd;
            this._inputElement.innerText = this._inputElement.innerText.substring(0, startPos) +
                value +
                this._inputElement.innerText.substring(endPos, this._inputElement.innerText.length);
            this._inputElement.selectionStart = startPos + value.length;
            this._inputElement.selectionEnd = startPos + value.length;
        } else {
            this._inputElement.innerText += value;
        }
        this._inputElement.focus();
    }

    evaluateEquation(equation) {
        if (this._outputElement) {
            equation = equation.replace(/×/g, "*");
            equation = equation.replace(/÷/g, "/");
            equation = equation.replace(/\./g, "");
            equation = equation.replace(/,/g, ".");

            let result = "";
            try {
                result = mathjs.evaluate(equation);
            } catch (e) {
                result = e.message;
            }

            if (result === undefined) {
                result = "";
            }

            result = "" + result;
            result = result.replace(/\./, ",");
            this._outputElement.innerText = result;
        }
    }
}

Calculator.TIME_LONG_PRESS = 1000;